import 'package:flutter/material.dart';

import '../../generated/l10n.dart';

class RegisterBusinessPartnerScreen extends StatelessWidget {
  RegisterBusinessPartnerScreen({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final internalization = S.of(context);
    return Scaffold(
      body: Center(
        child: Container(
            width: 550,
            decoration: const BoxDecoration(
              image: DecorationImage(
                  alignment: Alignment.center,
                  image: AssetImage("assets/images/fondo_total.png"),
                  fit: BoxFit.cover),
            ),
            padding: const EdgeInsets.all(15),
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children:[
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children:  [
                Container(
                    margin: const EdgeInsets.symmetric(vertical: 30),
                    child: Image.asset('assets/images/logo-purple.png', width: 140)),
                const Text("Persona Juridica", style: TextStyle(fontSize: 22)),
                Container(
                    margin: const EdgeInsets.symmetric(vertical: 10),
                    child: Image.asset('assets/images/persons/person_natural.png', width: 340)),
                Container(
                    margin: const EdgeInsets.only(bottom: 20),
                    width: 290,
                    child: const Text("Si quieres ser uno de nuestros aliados comerciales debes activar Business Partner aquí",  textAlign: TextAlign.center)),

                MaterialButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    disabledColor: const Color(0xffc7a314),
                    focusColor: const Color(0xffc7a314),
                    splashColor: const Color(0xffc7a314),
                    highlightColor: const Color(0xffEDCF53),
                    elevation: 0,
                    color: const Color(0xffEDCF53),
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 20, vertical: 15),
                      child: const Text('ACTIVAR BUSINESS PARTNER',
                          style: TextStyle(
                              color: Color(0xff6C4F92),
                              fontWeight: FontWeight.w700)),
                    ),
                    onPressed: () {
                        Navigator.pushNamed(context, 'business_partner');
                    }),
                Container(
                  margin: const EdgeInsets.symmetric(vertical: 5),
                  width: double.infinity,
                  child: IconButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      icon: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          const Icon(Icons.arrow_back_ios_new,
                              size: 14, color: Colors.black45),
                          Text(internalization.back,
                              style: const TextStyle(
                                  color: Colors.black45,
                                  fontWeight: FontWeight.w700,
                                  fontSize: 14))
                        ],
                      )),
                ),
              ],
            ),
        ])
    ),
      ));
  }
}

import 'package:flutter/material.dart';

import '../../../../generated/l10n.dart';
import '../../../../ui/input_decorations.ui.dart';
import '../../../../widgets/form/date_picker_input.widgets.dart';
import '../../../../widgets/form/dropdown_input.widgets.dart';
import '../../../../widgets/form/radio_input.widgets.dart';
import '../../../../widgets/btnGroup.widget.dart';
import '../../../../widgets/header.widget.dart';

class InformationGeneralRepresentanteScreen extends StatefulWidget {
  PageController pageController;
  int pageChanged;
  S internalization;


  InformationGeneralRepresentanteScreen(
      this.pageController, this.pageChanged, this.internalization);

  @override
  State<InformationGeneralRepresentanteScreen> createState() => _InformationGeneralRepresentanteScreenState();
}

class _InformationGeneralRepresentanteScreenState extends State<InformationGeneralRepresentanteScreen> {
  TextEditingController dateInputDate = TextEditingController();
  TextEditingController dropDownSelect = TextEditingController();

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final AutovalidateMode _autoValidateMode = AutovalidateMode.always;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Form(
        key: _formKey,
        autovalidateMode: _autoValidateMode,
        child: Container(
          margin: const EdgeInsets.all(0),
          color: Colors.transparent,
          child: Column(
            children: [
              HeaderFormWidget(
                stepNumber: '02',
                title: widget.internalization.infoGeneral,
                subTitle: widget.internalization.representLegal,
              ),
              TextFormField(
                enableSuggestions: false,
                cursorColor: Colors.grey,
                autocorrect: false,
                style: const TextStyle(color: Colors.black45),
                keyboardType: TextInputType.text,
                decoration: InputDecorations.generalInputDecoration(
                    hinText: '',
                    labelText: 'Nombres',
                    prefixIcon: Icons.account_circle,
                    colorInput: Colors.grey,
                    colorError: const Color(0xFFC21839)),
                textInputAction: TextInputAction.next,
                onEditingComplete: () => FocusScope.of(context).nextFocus(),
                validator: (String? value){
                  if(value == null || value.trim().isEmpty){
                    return "Ingresa el nombre completo";
                  }
                  return null;
                },
              ),
              const SizedBox(height: 8),
              TextFormField(
                enableSuggestions: false,
                cursorColor: Colors.grey,
                autocorrect: false,
                style: const TextStyle(color: Colors.black45),
                keyboardType: TextInputType.text,
                decoration: InputDecorations.generalInputDecoration(
                    hinText: '',
                    labelText: 'Apellidos',
                    prefixIcon: Icons.account_circle,
                    colorInput: Colors.grey,
                    colorError: const Color(0xFFC21839)),
                textInputAction: TextInputAction.next,
                onEditingComplete: () => FocusScope.of(context).nextFocus(),
                validator: (String? value){
                  if(value == null || value.trim().isEmpty){
                    return "Ingresa el apellido completo";
                  }
                  return null;
                },
              ),
              const SizedBox(height: 8),
              DopDownInput(
                list: [''],
                labelDocument: 'Tipo de documento.',
                dropDownSelect: dropDownSelect,
                prefixIcon: Icons.file_copy_rounded,
              ),
              const SizedBox(height: 8),
              TextFormField(
                enableSuggestions: false,
                cursorColor: Colors.grey,
                autocorrect: false,
                style: const TextStyle(color: Colors.black45),
                keyboardType: TextInputType.text,
                decoration: InputDecorations.generalInputDecoration(
                    hinText: '0123456789',
                    labelText: 'No. de identificación',
                    prefixIcon: Icons.numbers,
                    colorInput: Colors.grey,
                    colorError: const Color(0xFFC21839)),
                textInputAction: TextInputAction.next,
                onEditingComplete: () => FocusScope.of(context).nextFocus(),
                validator: (String? value){
                  if(value == null || value.trim().isEmpty){
                    return "Ingresa el número de identificación";
                  }
                  return null;
                },
              ),
              const SizedBox(height: 8),
              DatePickerInput(
                  textLabel: 'Fecha de expedición', dataInputDate: dateInputDate),
              const SizedBox(height: 8),
              DopDownInput(
                list: [''],
                labelDocument: 'Lugar de expedición',
                dropDownSelect: dropDownSelect,
                prefixIcon: Icons.location_on,
              ),
              const SizedBox(height: 8),
              Row(
                children: [
                  const Expanded(
                    flex: 1,
                    child: Text('TIENE VINCULOS COMERCIALES CON EL ESTADO',
                        style: TextStyle(fontSize: 10)),
                  ),
                  Expanded(
                      flex: 1,
                      child: RadioInput(
                        listRadio: ['Sí', 'No'],
                      )),
                ],
              ),
              const SizedBox(height: 25),
              BtnGroupWidget(
                pageController: widget.pageController,
                pageChanged: widget.pageChanged,
                internalization: widget.internalization,
              ),
              const SizedBox(height: 25),
            ],
          ),
        ),
      ),
    );
  }
}

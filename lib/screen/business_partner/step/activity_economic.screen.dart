import 'package:flutter/material.dart';

import '../../../../generated/l10n.dart';
import '../../../../ui/input_decorations.ui.dart';
import '../../../../widgets/form/radio_input.widgets.dart';
import '../../../../widgets/btnGroup.widget.dart';
import '../../../../widgets/header.widget.dart';

class ActivictyEconomicScreen extends StatelessWidget {
  PageController pageController;
  int pageChanged;
  S internalization;

  ActivictyEconomicScreen(this.pageController, this.pageChanged, this.internalization);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        color: Colors.transparent,
        child: Column(
          children: [
            HeaderFormWidget(
              stepNumber: '03',
              title: internalization.activityEconomic,
              subTitle: internalization.detailsEconomicActivity,
            ),
            TextFormField(
              enableSuggestions: false,
              cursorColor: Colors.grey,
              autocorrect: false,
              style: const TextStyle(color: Colors.black45),
              keyboardType: TextInputType.text,
              decoration: InputDecorations.generalInputDecoration(
                  hinText: 'Código CIIU',
                  labelText: internalization.activityEconomic,
                  prefixIcon: Icons.domain,
                  colorInput: Colors.grey),
              textInputAction: TextInputAction.next,
              onEditingComplete: () => FocusScope.of(context).nextFocus(),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                    margin:
                    const EdgeInsets.symmetric(horizontal: 0, vertical: 15),
                    child: const Text('NATURALEZA JURÍDICA:',
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.w700),
                        textAlign: TextAlign.left)),
                RadioInput(listRadio: const [
                  'Limitada',
                  'Anónima',
                  'Sociedad Extranjera',
                  'S.A.S.',
                  'Otro'
                ]),
                Container(
                    margin:
                    const EdgeInsets.symmetric(horizontal: 0, vertical: 15),
                    child: const Text('ACTIVIDAD PRINCIPAL:',
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.w700),
                        textAlign: TextAlign.left)),
                RadioInput(listRadio: const [
                  'Comercio',
                  'Servicios',
                  'Manufactura',
                  'Construcción',
                  'Otro'
                ]),
                Container(
                    margin:
                    const EdgeInsets.symmetric(horizontal: 0, vertical: 15),
                    child: const Text('NICHO DE MERCADO:',
                        style:TextStyle(
                            fontSize: 18, fontWeight: FontWeight.w700),
                        textAlign: TextAlign.left)),
                RadioInput(listRadio: const [
                  'Generador contenido',
                  'Desarrollador',
                  'Influencer',
                  'Microempresario',
                  'Casino',
                  'Profesional independiente',
                  'Bar',
                  'Gamer',
                  'Otro'
                ]),
              ],
            ),
            const SizedBox(height: 25),
            BtnGroupWidget(pageController: pageController, pageChanged: pageChanged, internalization: internalization,),
            const SizedBox(height: 25),
          ],
        ),
      ),
    );
  }
}

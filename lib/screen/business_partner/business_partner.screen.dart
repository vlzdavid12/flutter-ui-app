import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:xisfo_app/screen/business_partner/step/activity_economic.screen.dart';
import 'package:xisfo_app/screen/business_partner/step/declaration_origen_fondo.screen.dart';
import 'package:xisfo_app/screen/business_partner/step/end_contract_money.screen.dart';
import 'package:xisfo_app/screen/business_partner/step/information_basica.screen.dart';
import 'package:xisfo_app/screen/business_partner/step/information_general_representante.screen.dart';
import 'package:xisfo_app/screen/business_partner/step/investors.screen.dart';

import 'package:xisfo_app/bloc/investor/investor_bloc.dart';
import 'package:xisfo_app/generated/l10n.dart';
import 'package:xisfo_app/helpers/helpers.dart';

class BusinessPartnerScreen extends StatefulWidget {
  @override
  State<BusinessPartnerScreen> createState() => _BusinessPartnerScreenState();
}

class _BusinessPartnerScreenState extends State<BusinessPartnerScreen> {
  PageController pageController = PageController();

  late int pageChanged = 0;

  @override
  void initState() {
    super.initState();
    changeStatusLight();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final internalization = S.of(context);

    return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          centerTitle: true,
          toolbarHeight: 60,
          backgroundColor: Colors.white,
          elevation: 0,
          title: Container(
              padding: const EdgeInsets.only(top: 5),
              height: 48,
              child: Image.asset('assets/images/logo-purple.png')),
        ),
        body: SingleChildScrollView(
          child: MultiBlocProvider(
            providers: [BlocProvider(create: (_) => InvestorBloc())],
            child: Center(
                child: Container(
              padding: const EdgeInsets.all(15),
              width: 590,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                      width: double.infinity,
                      height: size.height / 1.3,
                      child: PageView(
                          physics: const NeverScrollableScrollPhysics(),
                          pageSnapping: true,
                          controller: pageController,
                          onPageChanged: (index) {
                            setState(() {
                              pageChanged = index;
                            });
                          },
                          children: [
                            InformationBasicScreen(
                                pageController, pageChanged, internalization),
                            InformationGeneralRepresentanteScreen(
                                pageController, pageChanged, internalization),
                            ActivictyEconomicScreen(pageController, pageChanged, internalization),
                            InvestorsScreen(pageController, pageChanged, internalization),
                            DeclarationOrigenFondoScreen(pageController, pageChanged, internalization),
                            EndContractTypeMoneyScreen(
                                pageController, pageChanged, internalization)
                          ])),
                  _Dots(6, pageChanged),
                ],
              ),
            )),
          ),
        ));
  }
}

class _Dots extends StatelessWidget {
  final int totalSlides;
  final int pageChanged;

  const _Dots(this.totalSlides, this.pageChanged);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.transparent,
      child: SizedBox(
        width: double.infinity,
        height: 35,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: List.generate(totalSlides, (i) => _Dot(i, pageChanged)),
        ),
      ),
    );
  }
}

class _Dot extends StatelessWidget {
  final int index;
  final int pageChanged;
  late Color color;
  late double widthBox;

  _Dot(this.index, this.pageChanged);

  @override
  Widget build(BuildContext context) {
    if (pageChanged >= index - 0.5 && pageChanged < index + 0.5) {
      color = const Color(0xff6C4F92);
      widthBox = 8;
    } else {
      color = Colors.grey;
      widthBox = 5;
    }

    return AnimatedContainer(
      duration: const Duration(milliseconds: 200),
      width: 28,
      height: widthBox,
      margin: const EdgeInsets.symmetric(horizontal: 5),
      decoration: BoxDecoration(
          color: color, //Colors.grey
          borderRadius: BorderRadius.circular(10)),
    );
  }
}

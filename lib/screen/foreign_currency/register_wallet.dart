import 'package:animate_do/animate_do.dart';
import "package:flutter/material.dart";
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lottie/lottie.dart';
import 'package:xisfo_app/generated/l10n.dart';

import '../../bloc/foreign_currency/foreign_currency_bloc.dart';

class RegisterWallet extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final internalization = S.of(context);
    final size = MediaQuery.of(context).size;
    const isPersonNature = false;
    return Scaffold(
        body: Center(
      child: Container(
        width: 550,
        decoration: const BoxDecoration(
          image: DecorationImage(
              alignment: Alignment.center,
              image: AssetImage("assets/images/fondo_total.png"),
              fit: BoxFit.cover),
        ),
        padding: const EdgeInsets.all(15),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            FadeIn(
                duration: const Duration(milliseconds: 2000),
                child: Image.asset('assets/images/logo-purple.png', width: 110)),
            const SizedBox(height: 8),
            isPersonNature ? Column(
              mainAxisAlignment:  MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 20),
                  child: Text(internalization.personLegal, style: const TextStyle(fontSize: 23, color: Colors.black54
                  ), textAlign: TextAlign.center,),
                ),
                Container(
                    width: size.width / 1.3,
                    child: FadeIn(
                        duration: const Duration(milliseconds: 2000),
                        child: Lottie.asset(
                          'assets/images/persons/person_2.json',
                          fit: BoxFit.fill,
                        ))),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                  child: FadeIn(
                    duration: const Duration(milliseconds: 2000),
                    child: Text(internalization.textMoney, style: const TextStyle(fontSize: 14, color: Colors.black54
                    ), textAlign: TextAlign.center,),
                  ),
                ),
                FadeInUp(
                  child: MaterialButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(50)),
                      disabledColor: const Color(0xffc7a314),
                      focusColor: const Color(0xffc7a314),
                      splashColor: const Color(0xffc7a314),
                      highlightColor: const Color(0xffEDCF53),
                      elevation: 0,
                      color: const Color(0xffEDCF53),
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 20, vertical: 15),
                        child: const Text("ACTIVAR",
                            style: TextStyle(
                                color: Color(0xff6C4F92),
                                fontWeight: FontWeight.w700)),
                      ),
                      onPressed: () {
                        context.read<ForeignCurrencyBloc>().add(const IsCurrencyEvent(isCurrency: "foreign_currency"));
                        Navigator.pushNamed(context, 'person_juridica');
                      }),
                ),
              ],
            ) : Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 20),
                  child: Text(internalization.personNature, style: const TextStyle(fontSize: 23, color: Colors.black54
                  ), textAlign: TextAlign.center,),
                ),
                Container(
                    width: size.width / 1.5,
                    child: FadeIn(
                        duration: const Duration(milliseconds: 2000),
                        child: Lottie.asset(
                          'assets/images/persons/person_3.json',
                          fit: BoxFit.fill,
                        ))),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                  child: FadeIn(
                    duration: const Duration(milliseconds: 2000),
                    child: Text(internalization.textMoney, style: const TextStyle(fontSize: 14, color: Colors.black54
                    ), textAlign: TextAlign.center,),
                  ),
                ),
                FadeInUp(
                  child: MaterialButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(50)),
                      disabledColor: const Color(0xffc7a314),
                      focusColor: const Color(0xffc7a314),
                      splashColor: const Color(0xffc7a314),
                      highlightColor: const Color(0xffEDCF53),
                      elevation: 0,
                      color: const Color(0xffEDCF53),
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 20, vertical: 15),
                        child: Text('ACTIVAR',
                            style: const TextStyle(
                                color: Color(0xff6C4F92),
                                fontWeight: FontWeight.w700)),
                      ),
                      onPressed: () {
                        context.read<ForeignCurrencyBloc>().add(const IsCurrencyEvent(isCurrency: "foreign_currency"));
                        Navigator.pushNamed(context, 'person_natural');
                      }),
                ),
              ],
            ),
            Container(
              margin: const EdgeInsets.symmetric(vertical: 5),
              width: double.infinity,
              child: IconButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  icon: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const Icon(Icons.arrow_back_ios_new,
                          size: 14, color: Colors.black45),
                      Text(internalization.back,
                          style: const TextStyle(
                              color: Colors.black45,
                              fontWeight: FontWeight.w700,
                              fontSize: 14))
                    ],
                  )),
            ),
          ],
        ),
      ),
    ));
  }
}

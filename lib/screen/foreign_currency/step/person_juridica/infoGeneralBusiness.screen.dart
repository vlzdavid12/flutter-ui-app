import "package:flutter/material.dart";

import '../../../../generated/l10n.dart';
import '../../../../ui/input_decorations.ui.dart';
import '../../../../widgets/form/dropdown_input.widgets.dart';
import '../../../../widgets/btnGroup.widget.dart';
import '../../../../widgets/header.widget.dart';

class StepOne extends StatefulWidget {
  PageController pageController;
  int pageChanged;
  S internalization;


  StepOne(this.pageController, this.pageChanged, this.internalization);

  @override
  State<StepOne> createState() => _StepOneState();
}

class _StepOneState extends State<StepOne> {
  TextEditingController dropDownSelect = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final AutovalidateMode _autoValidateMode = AutovalidateMode.always;
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Form(
        key: _formKey,
        autovalidateMode: _autoValidateMode,
        child: Container(
          margin: const EdgeInsets.all(0),
          color: Colors.transparent,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              HeaderFormWidget(
                stepNumber: '01',
                title: widget.internalization.infoGeneral,
                subTitle: widget.internalization.youBusiness,
              ),
              TextFormField(
                enableSuggestions: false,
                cursorColor: Colors.grey,
                autocorrect: false,
                style: const TextStyle(color: Colors.black45),
                keyboardType: TextInputType.text,
                decoration: InputDecorations.generalInputDecoration(
                    hinText: 'Razón social',
                    labelText: 'Nombre legal- Empresa',
                    prefixIcon: Icons.account_circle,
                    colorInput: Colors.grey,
                    colorError: const Color(0xFFC21839)
                ),
                textInputAction: TextInputAction.next,
                onEditingComplete: () => FocusScope.of(context).nextFocus(),
                validator: (String? value){
                  if(value == null || value.trim().isEmpty){
                    return "Ingresa el nombre completo de la empresa.";
                  }
                  return null;
                },
              ),
              const SizedBox(height: 8),
              Container(
                margin: const EdgeInsets.only(bottom: 4),
                child: DopDownInput(list: [''], labelDocument: 'Tipo de documento',  dropDownSelect: dropDownSelect, prefixIcon: Icons.file_copy_sharp,),
              ),
              const SizedBox(height: 8),
              TextFormField(
                enableSuggestions: false,
                cursorColor: Colors.grey,
                autocorrect: false,
                style: const TextStyle(color: Colors.black45),
                keyboardType: TextInputType.text,
                decoration: InputDecorations.generalInputDecoration(
                    hinText: '0123456789',
                    labelText: 'Número de identificación',
                    prefixIcon: Icons.numbers,
                    colorInput: Colors.grey,
                    colorError: const Color(0xFFC21839)
                ),
                textInputAction: TextInputAction.next,
                onEditingComplete: () => FocusScope.of(context).nextFocus(),
                validator: (String? value){
                  if(value == null || value.trim().isEmpty){
                    return "Ingresa el número de identificación.";
                  }
                  return null;
                },

              ),
              const SizedBox(height: 8),
              TextFormField(
                enableSuggestions: false,
                cursorColor: Colors.grey,
                autocorrect: false,
                style: const TextStyle(color: Colors.black45),
                keyboardType: TextInputType.text,
                decoration: InputDecorations.generalInputDecoration(
                    hinText: 'Cra. 45 No. 90 - 89',
                    labelText: 'Dirección de domicilio',
                    prefixIcon: Icons.numbers,
                    colorInput: Colors.grey,
                    colorError: const Color(0xFFC21839)
                ),
                textInputAction: TextInputAction.next,
                onEditingComplete: () => FocusScope.of(context).nextFocus(),
                validator: (String? value){
                  if(value == null || value.trim().isEmpty){
                    return "Ingresa la dirección de residencia.";
                  }
                  return null;
                },
              ),
              const SizedBox(height: 8),
              DopDownInput(list: [''], labelDocument: 'País',  dropDownSelect: dropDownSelect, prefixIcon: Icons.public),
              const SizedBox(height: 8),
              DopDownInput(list: [''], labelDocument: 'Ciudad',  dropDownSelect: dropDownSelect, prefixIcon: Icons.public),
              const SizedBox(height: 8),
              TextFormField(
                enableSuggestions: false,
                cursorColor: Colors.grey,
                autocorrect: false,
                style: const TextStyle(color: Colors.black45),
                keyboardType: TextInputType.phone,
                decoration: InputDecorations.generalInputDecoration(
                    hinText: '000 000 0000',
                    labelText: 'Número celular',
                    prefixIcon: Icons.phone,
                    colorInput: Colors.grey,
                    colorError: const Color(0xFFC21839)),
                textInputAction: TextInputAction.next,
                onEditingComplete: () => FocusScope.of(context).nextFocus(),
                validator: (String? value){
                  if(value == null || value.trim().isEmpty){
                    return "Ingresa el telefóno mobil.";
                  }
                  return null;
                },

              ),
              const SizedBox(height: 8),
              TextFormField(
                enableSuggestions: false,
                cursorColor: Colors.grey,
                autocorrect: false,
                style: const TextStyle(color: Colors.black45),
                keyboardType: TextInputType.emailAddress,
                decoration: InputDecorations.generalInputDecoration(
                    hinText: 'micorreo@xisfo.co',
                    labelText: 'Correo electrónico',
                    prefixIcon: Icons.email,
                    colorInput: Colors.grey,
                    colorError: const Color(0xFFC21839)),
                textInputAction: TextInputAction.next,
                onEditingComplete: () => FocusScope.of(context).nextFocus(),
                validator: (String? value){
                  if(value == null || value.trim().isEmpty){
                    return "Ingresa el correo electrónico.";
                  }
                  return null;
                },
              ),
              BtnGroupWidget(pageController: widget.pageController, pageChanged: widget.pageChanged, internalization: widget.internalization,),
            ],
          ),
        ),
      ),
    );
  }
}

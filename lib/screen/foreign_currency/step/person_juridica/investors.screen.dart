import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:xisfo_app/widgets/btnGroup.widget.dart';

import '../../../../bloc/investor/investor_bloc.dart';
import '../../../../generated/l10n.dart';
import '../../../../ui/input_decorations.ui.dart';
import '../../../../widgets/form/date_picker_input.widgets.dart';
import '../../../../widgets/form/dropdown_input.widgets.dart';
import '../../../../widgets/header.widget.dart';
import '../../../../widgets/modal.widgets.dart';

class StepFour extends StatelessWidget {
  PageController pageController;
  int pageChanged;
  S internalization;

  StepFour(this.pageController, this.pageChanged, this.internalization);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return InvestorView(
            internalization: internalization,
            pageChanged: pageChanged,
            size: size,
            pageController: pageController);
  }
}

class InvestorView extends StatelessWidget {
  const InvestorView({
    required this.internalization,
    required this.pageChanged,
    required this.size,
    required this.pageController,
  });

  final S internalization;
  final int pageChanged;
  final Size size;
  final PageController pageController;

  @override
  Widget build(BuildContext context) {
    final listInvestors = context.watch<InvestorBloc>().state.investors;
    var countStake = 0;
    for (var element in listInvestors) {
         countStake += int.parse(element.stake);
    }
    return SingleChildScrollView(
      child: Container(
          color: Colors.transparent,
          child: Column(children: [
            HeaderFormWidget(
              stepNumber: '04',
              title: internalization.investor,
              subTitle: internalization.investorSubtitle,
            ),
            MaterialButton(
                minWidth: pageChanged >= 1 && pageChanged <= 4 ? 140 : 320,
                height: 50,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
                disabledColor: const Color(0xffc7a314),
                focusColor: const Color(0xffc7a314),
                splashColor: const Color(0xffc7a314),
                highlightColor: const Color(0xffEDCF53),
                elevation: 0,
                color: const Color(0xffEDCF53),
                onPressed: () {
                  _showAddModal(context);
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(internalization.addShareHolder.toUpperCase(),
                        style: const TextStyle(
                            color: Color(0xff6C4F92),
                            fontWeight: FontWeight.w700,
                            height: 1.4)),
                    const SizedBox(width: 8),
                    const Icon(Icons.add_circle_outline,
                        color: Color(0xff6C4F92))
                  ],
                )),
            Container(
                width: double.infinity,
                height: size.height / 2.4,
                margin: const EdgeInsets.symmetric(vertical: 2.5),
                child: ListView.builder(
                    shrinkWrap: true,
                    itemCount: listInvestors.length,
                    itemBuilder: (context, index) {
                      final item = listInvestors[index];
                      return Dismissible(
                        direction: DismissDirection.endToStart,
                        key: UniqueKey(),
                        onDismissed: (DismissDirection direction) {
                          if (direction == DismissDirection.endToStart) {
                            context.read<InvestorBloc>().add(RemoveInvestorEvent(investor: item));
                          }
                        },
                        background: Container(
                          alignment: AlignmentDirectional.centerEnd,
                          color: const Color(0xFF6D3DB0),
                          child: const Padding(
                            padding: EdgeInsets.fromLTRB(0.0, 0.0, 30.0, 0.0),
                            child: Icon(
                              Icons.delete,
                              color: Colors.white,
                            ),
                          ),
                        ),
                        child: ListTile(
                          leading: Text('${item.stake} %', style: const TextStyle(height: 1.5, fontSize: 22, fontWeight: FontWeight.w700, color: Colors.black87),),
                          title: Text(item.investor),
                        ),
                      );
                    })),
            Container(
              decoration:  BoxDecoration(
                  borderRadius: const BorderRadius.all(Radius.circular(5)),
                  color: (countStake >= 101) ? Colors.red : const Color(0xffEDCF53)),
              padding: const EdgeInsets.all(10),
              margin: const EdgeInsets.all(10),
              child: Text(internalization.appInvestorTotal('$countStake%'), style: TextStyle(color:  (countStake >= 100) ? Colors.white : Color(0xff6C4F92)),),
            ),
            BtnGroupWidget(
              pageController: pageController,
              pageChanged: pageChanged,
              internalization: internalization,
            ),
            const SizedBox(height: 15),
          ])),
    );
  }
}

Future<void> _showAddModal(BuildContext context) async {
  final GlobalKey<FormState> formKeyInvestors = GlobalKey<FormState>();
  final internalization = S.of(context);

  TextEditingController dateInputDate = TextEditingController();
  TextEditingController valuePeps = TextEditingController();
  TextEditingController valueTypeDocument = TextEditingController();

  String? _nameFull, _noIdentification, _staKens;

  void _addSubmit() {
    final form = formKeyInvestors.currentState;
    if (form == null || !form.validate()) return;
    form.save();

    context.read<InvestorBloc>().add(AddInvestorEvent(
        dateExpedition: dateInputDate.text,
        stake: _staKens!,
        numberIdentification: _noIdentification!,
        peps: false,
        investor: _nameFull!));

    Navigator.of(context).pop();
  }

  final childContent = Form(
    key: formKeyInvestors,
    autovalidateMode: AutovalidateMode.onUserInteraction,
    child: Column(children: [
      Container(
        decoration: const BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(5)),
          color: Color(0xffEDCF53),
        ),
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 15),
        margin: const EdgeInsets.only(bottom: 10),
        child: Text(
          internalization.alertInvestorConfirm,
          style: const TextStyle(fontSize: 11, color: Colors.white),
          textAlign: TextAlign.center,

        ),
      ),
      Padding(
        padding: const EdgeInsets.symmetric(vertical: 5),
        child: TextFormField(
          enableSuggestions: false,
          cursorColor: Colors.grey,
          autocorrect: false,
          style: const TextStyle(color: Colors.black45),
          keyboardType: TextInputType.text,
          decoration: InputDecorations.generalInputDecoration(
              hinText: '',
              labelText: 'Nombre completo o Razón social',
              prefixIcon: Icons.business,
              colorError: const Color(0xFFC21839),
              colorInput: Colors.grey),
          validator: (String? value) {
            if (value == null || value.trim().isEmpty) {
              return "Este campo es requerido.";
            }
            if (value.trim().length <= 3) {
              return "Este campo debe ser mayor a 3 carácteres.";
            }
            return null;
          },
          onSaved: (String? value) {
            _nameFull = value;
          },
        ),
      ),
      Padding(
        padding: const EdgeInsets.symmetric(vertical: 5),
        child: DopDownInput(
            list: ['tipo'],
            labelDocument: "Tipo Documento",
            dropDownSelect: valueTypeDocument, prefixIcon: Icons.file_copy_outlined,),
      ),
      TextFormField(
        enableSuggestions: false,
        cursorColor: Colors.grey,
        autocorrect: false,
        style: const TextStyle(color: Colors.black45),
        keyboardType: TextInputType.text,
        decoration: InputDecorations.generalInputDecoration(
            hinText: '',
            labelText: 'N° de identificación',
            prefixIcon: Icons.numbers,
            colorInput: Colors.grey,
            colorError: const Color(0xFFC21839)),
        validator: (String? value) {
          if (value == null || value.trim().isEmpty) {
            return "Este campo es requerido.";
          }
          return null;
        },
        onSaved: (String? value) {
          _noIdentification = value;
        },
      ),
      Padding(
        padding: const EdgeInsets.symmetric(vertical: 5),
        child: DatePickerInput(
            textLabel: 'Fecha de expedición', dataInputDate: dateInputDate),
      ),
      Padding(
        padding: const EdgeInsets.symmetric(vertical: 5),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
                flex: 1,
                child: Container(
                  padding: const EdgeInsets.only(right: 10),
                  child: TextFormField(
                    enableSuggestions: false,
                    cursorColor: Colors.grey,
                    autocorrect: false,
                    style: const TextStyle(color: Colors.black45),
                    keyboardType: TextInputType.number,
                    decoration: InputDecorations.generalInputDecoration(
                        hinText: '',
                        labelText: 'Participación',
                        prefixIcon: Icons.percent,
                        colorError: const Color(0xFFC21839),
                        colorInput: Colors.grey),
                    onSaved: (String? value) {
                      _staKens = value;
                    },
                    validator: (String? value) {
                      if (value == null || value.trim().isEmpty) {
                        return "Este campo es requerido.";
                      }

                      if (int.parse(value) > 100) {
                        return "Minimo es 100";
                      }
                      return null;
                    },
                  ),
                )),
            Expanded(
              child: DopDownInput(
                  list: const ['Sí', 'No'],
                  labelDocument: "PEP'S",
                  dropDownSelect: valuePeps, prefixIcon: Icons.local_police,),
            ),
          ],
        ),
      ),
      MaterialButton(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          disabledColor: const Color(0xffa18bc7),
          focusColor: const Color(0xff4d2d79),
          splashColor: const Color(0xff53298a),
          highlightColor: const Color(0xff6C4F92),
          elevation: 0,
          color: const Color(0xff6C4F92),
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 80, vertical: 15),
            child: const Text('AÑADIR',
                style: TextStyle(
                    color: Colors.white, fontWeight: FontWeight.w700)),
          ),
          onPressed: () => _addSubmit()),
    ]),
  );
  await dialogModalBuilder(context, childContent);
}

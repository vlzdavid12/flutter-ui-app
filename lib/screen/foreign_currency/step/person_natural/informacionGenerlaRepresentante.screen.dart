import "package:flutter/material.dart";
import 'package:xisfo_app/widgets/btnGroup.widget.dart';

import "../../../../generated/l10n.dart";
import "../../../../ui/input_decorations.ui.dart";
import "../../../../widgets/form/radio_input.widgets.dart";
import "../../../../widgets/header.widget.dart";

class StepTwo extends StatelessWidget {
  PageController pageController;
  int pageChanged;
  S internalization;

  StepTwo(this.pageController, this.pageChanged, this.internalization);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        margin: const EdgeInsets.all(0),
        color: Colors.transparent,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment:  MainAxisAlignment.start,
          children: [
            HeaderFormWidget(
              stepNumber: '02',
              title: internalization.infoGeneral,
              subTitle: internalization.detailActivityEconomic,
            ),
            TextFormField(
              enableSuggestions: false,
              cursorColor: Colors.grey,
              autocorrect: false,
              style: const TextStyle(color: Colors.black45),
              keyboardType: TextInputType.text,
              decoration: InputDecorations.generalInputDecoration(
                  hinText: 'Código CIIU',
                  labelText: internalization.activityEconomic,
                  prefixIcon: Icons.business,
                  colorInput: Colors.grey),
            ),
            Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                child: const Text('NATURALEZA JURÍDICA:', style: TextStyle(fontSize: 18, color:  Colors.black54, fontWeight: FontWeight.w700),)),
            RadioInput(
              listRadio: ['Régimen Simplificado', 'Régimen Común'],
            ),
            Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                child: const Text('ACTIVIDAD PRINCIPAL:', style: TextStyle(fontSize: 18, color:  Colors.black54, fontWeight: FontWeight.w700),)),
            RadioInput(
              listRadio: ['Comercio', 'Manufactura', 'Construcción', 'Servicios', 'Otro'],
            ),
            Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                child: const Text('NICHO MERCADO:', style: TextStyle(fontSize: 18, color:  Colors.black54, fontWeight: FontWeight.w700),)),
            RadioInput(
              listRadio: ['Generador contenido', 'Influencer', 'Casino', 'Bar', 'Gamer', 'Desarrollador', 'Microempresario', 'Profesional independiente', 'Otro'],
            ),
            const SizedBox(height: 25),
            BtnGroupWidget(pageController: pageController, pageChanged: pageChanged, internalization: internalization,),
            const SizedBox(height: 25),
          ],
        ),
      ),
    );
  }
}

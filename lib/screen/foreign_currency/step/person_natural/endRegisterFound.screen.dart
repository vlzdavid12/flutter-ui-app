import "package:flutter/material.dart";
import "package:flutter_html/flutter_html.dart";
import 'package:xisfo_app/widgets/btnGroup.widget.dart';

import "../../../../generated/l10n.dart";
import "../../../../ui/contract_legal_natural_person.dart";
import "../../../../widgets/form/checkbox_input.widgets.dart";
import "../../../../widgets/header.widget.dart";

class StepThree extends StatelessWidget {
  PageController pageController;
  int pageChanged;
  S internalization;

  StepThree(this.pageController, this.pageChanged, this.internalization);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return SingleChildScrollView(
      child: Container(
        color: Colors.transparent,
        child: Column(
          children: [
            HeaderFormWidget(
              stepNumber: '03',
              title: internalization.endRegister,
              subTitle: internalization.endStatementFound,
            ),
            SizedBox(
              height: size.height / 3.2,
              child: SingleChildScrollView(
                  child: Html(data: TermContractLegal.contractDeclarationFound())),
            ),
            CheckBoxInput(lisTitles:  [
              internalization.declarationOrigenFound
            ]),
            CheckBoxInput(lisTitles:[
              internalization.leyPoliData
            ]),

            const SizedBox(height: 25),
            BtnGroupWidget(pageController: pageController, pageChanged: pageChanged, internalization: internalization,),
            const SizedBox(height: 25),
          ],
        ),
      ),
    );
  }
}
import 'dart:io' show Platform;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:xisfo_app/bloc/notify/notify_bloc.dart';
import 'package:xisfo_app/generated/l10n.dart';
import 'package:xisfo_app/models/list_notify.model.dart';
import 'package:xisfo_app/widgets/layout.widgets.dart';

class NotifyListScreen extends StatelessWidget {
  NotifyListScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final internalization = S.of(context);
    final listNotification = context.watch<NotifyBloc>().state.notify;

    return LayoutApp(
        children: SafeArea(
            child: listNotification.isNotEmpty
                ? ListView.builder(
                    itemCount: listNotification.length,
                    itemBuilder: (context, index) => Card(
                        elevation: 0,
                        margin: const EdgeInsets.symmetric(
                            vertical: 3, horizontal: 10),
                        child: _ListNotify(
                            listNotification: listNotification,
                            internalization: internalization,
                            index: index)))
                : Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [Text(internalization.noFoundNotify)])));
  }
}

class _ListNotify extends StatelessWidget {
  const _ListNotify({
    required this.index,
    required this.listNotification,
    required this.internalization,
  });

  final List<ListNotify> listNotification;
  final int index;
  final S internalization;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Image.asset('assets/images/list_icons/icon1.png', width: 50),
      title: Text(listNotification[index].title),
      subtitle: Text(listNotification[index].description),
      trailing: IconButton(
          onPressed: () => (Platform.isAndroid)
              ? showDialog<String>(
                  context: context,
                  builder: (BuildContext context) => AlertDialog(
                    title: Text('¿${internalization.deleteTextAlert}'),
                    content: Text(internalization.deleteTextAlert2),
                    actions: <Widget>[
                      TextButton(
                        style: ButtonStyle(
                          foregroundColor:
                              MaterialStateProperty.all<Color>(Colors.black),
                        ),
                        onPressed: () => Navigator.pop(context, 'Cancel'),
                        child: Text(internalization.notWant),
                      ),
                      TextButton(
                        style: ButtonStyle(
                          foregroundColor:
                              MaterialStateProperty.all<Color>(Colors.black),
                        ),
                        onPressed: () => {
                          context.read<NotifyBloc>().add(
                              RemoveNotify(notify: listNotification[index])),
                          Navigator.pop(context, 'OK')
                        },
                        child: Text(internalization.yesWant),
                      ),
                    ],
                  ),
                )
              : showDialog<String>(
                  context: context,
                  builder: (BuildContext context) => CupertinoAlertDialog(
                    title: Text('¿${internalization.deleteTextAlert}'),
                    content: Text(internalization.deleteTextAlert2),
                    actions: <CupertinoDialogAction>[
                      CupertinoDialogAction(
                        isDefaultAction: true,
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: Text(internalization.notWant),
                      ),
                      CupertinoDialogAction(
                        isDestructiveAction: true,
                        onPressed: () {
                          context.read<NotifyBloc>().add(
                              RemoveNotify(notify: listNotification[index]));
                          Navigator.pop(context);
                        },
                        child: Text(internalization.yesWant),
                      ),
                    ],
                  ),
                ),
          icon: const Icon(
            Icons.close,
            size: 20,
          )),
    );
  }
}

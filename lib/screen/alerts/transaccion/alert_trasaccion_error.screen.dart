import 'dart:math';

import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import '../../../helpers/helpers.dart';
import '../../../widgets/layout.widgets.dart';

class AlertTrasactionError extends StatelessWidget {
  final String title;
  final String description;
  final VoidCallback onTap;
  final String txtBtn;

  const AlertTrasactionError(
      {Key? key,
      required this.title,
      required this.description,
      required this.onTap,
      required this.txtBtn})
      : super(key: key);

  Widget build(BuildContext context) {
    final numberImage = Random().nextInt(2) + 1;
    changeStatusLight();
    return LayoutApp(
      children: Padding(
        padding: const EdgeInsets.all(8.0),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const SizedBox(
                height: 60,
              ),
              Swing(
                duration: const Duration(milliseconds: 1000),
                child: Image.asset(
                  'assets/images/icons/face_error_$numberImage.png',
                  width: 130,
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Text(
                title,
                textAlign: TextAlign.center,
                style: const TextStyle(
                    color: Color(0xff6C4F92),
                    fontSize: 44,
                    fontFamily: 'Dongle',
                    letterSpacing: -1,
                    height: 0.8),
              ),
              const SizedBox(height: 10),
              Text(
                description,
                style: const TextStyle(color: Colors.black45),
                textAlign: TextAlign.center,
              ),
              const SizedBox(
                height: 20,
              ),
              MaterialButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(50)),
                  disabledColor: const Color(0xffc7a314),
                  focusColor: const Color(0xffc7a314),
                  splashColor: const Color(0xffc7a314),
                  highlightColor: const Color(0xffEDCF53),
                  elevation: 0,
                  color: const Color(0xffEDCF53),
                  onPressed: onTap,
                  child: Container(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 40, vertical: 15),
                    child: Text(txtBtn,
                        style: const TextStyle(
                            color: Color(0xff6C4F92),
                            fontWeight: FontWeight.w700)),
                  )),
            ],
          ),
        ),
      ),
    );
  }
}

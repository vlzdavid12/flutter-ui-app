import 'dart:math';

import 'package:animate_do/animate_do.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:xisfo_app/generated/l10n.dart';
import 'package:xisfo_app/widgets/layout.widgets.dart';

class AlertSuccessFullyScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final internalization = S.of(context);
    final dateNow = DateTime.now();
    final numberImage = Random().nextInt(2) + 1;
    return LayoutApp(
        children: Padding(
      padding: const EdgeInsets.all(8.0),
      child: SingleChildScrollView(
        child:  Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const SizedBox(
              height: 60,
            ),
            Swing(
              duration: const Duration(milliseconds: 1000),
              child: Image.asset(
                'assets/images/icons/face_success_fully_$numberImage.png',
                width: 130,
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            const Text(
              '¡Transacción Exitosa!',
              style: TextStyle(
                  color: Color(0xff6C4F92),
                  fontSize: 44,
                  fontFamily: 'Dongle',
                  letterSpacing: -1,
                  height: 0.8),
            ),
            const SizedBox(height: 10),
            SizedBox(
              width: 300,
              height: 450,
              child: Stack(
                  children:[
                    DottedBorder(
                        padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 25),
                        borderType: BorderType.RRect,
                        radius: const Radius.circular(10),
                        dashPattern: const [4, 4],
                        strokeWidth: 2,
                        color: const Color(0xff6C4F92),
                        child: SizedBox(
                          height: 350,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(internalization.textPaymentSuccessFully, style: const TextStyle(color: Colors.black45, fontSize: 18), textAlign: TextAlign.center),
                              const SizedBox(height: 20),
                              Text(internalization.paymentHave("321 348 7589"),
                                  style: const TextStyle(fontSize: 15, color: Colors.black45)),
                              const SizedBox(height: 20),
                              Table(
                                border: const TableBorder(verticalInside: BorderSide.none),
                                columnWidths: const <int, TableColumnWidth>{
                                  0: FixedColumnWidth(110),
                                  1: FixedColumnWidth(140),
                                },
                                defaultVerticalAlignment: TableCellVerticalAlignment.middle,
                                children: <TableRow>[
                                  TableRow(
                                    children: <Widget>[
                                      Container(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 5, horizontal: 10),
                                        child: const Text("Mensaje", style: TextStyle(color: Colors.black45,  fontWeight: FontWeight.w700,  fontSize: 13)),
                                      ),
                                      Container(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 5, horizontal: 10),
                                        child: const Text("texto de solicitud", style: TextStyle(color: Colors.black45,  fontSize: 12)),
                                      ),
                                    ],
                                  ),
                                  TableRow(
                                    children: <Widget>[
                                      Container(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 5, horizontal: 10),
                                        child: const Text("Factura N°.", style: TextStyle(color: Colors.black45, fontWeight: FontWeight.w700,  fontSize: 13)),
                                      ),
                                      Container(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 5, horizontal: 10),
                                        child: const Text("F-908987", style: TextStyle(color: Colors.black45,  fontSize: 12)),
                                      ),
                                    ],
                                  ),
                                  TableRow(
                                    children: <Widget>[
                                      Container(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 5, horizontal: 10),
                                        child: const Text("Fecha y hora", style: TextStyle(color: Colors.black45,  fontWeight: FontWeight.w700,  fontSize: 13)),
                                      ),
                                      Container(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 5, horizontal: 10),
                                        child: Text(DateFormat('yyyy-mm-dd mm:ss').format(dateNow), style: const TextStyle(color: Colors.black45,  fontSize: 12)),
                                      ),
                                    ],
                                  ),
                                  TableRow(
                                    children: <Widget>[
                                      Container(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 5, horizontal: 10),
                                        child: const Text("Valor", style: TextStyle(color: Colors.black45, fontWeight: FontWeight.w700,  fontSize: 13),),
                                      ),
                                      Container(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 5, horizontal: 10),
                                        child: Text("733.15 USD", style: TextStyle(color: Colors.black45),),
                                      ),
                                    ],
                                  ),
                                  TableRow(
                                    children: <Widget>[
                                      Container(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 5, horizontal: 10),
                                        child: const Text("Procesamiento Bancario", style: TextStyle(color: Colors.black45, fontWeight: FontWeight.w700,  fontSize: 13),),
                                      ),
                                      Container(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 5, horizontal: 10),
                                        child: Text("5.00 USD", style: TextStyle(color: Colors.black45),),
                                      ),
                                    ],
                                  ),
                                  TableRow(
                                    children: <Widget>[
                                      Container(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 5, horizontal: 10),
                                        child: const Text("Comisión", style: TextStyle(color: Colors.black45, fontWeight: FontWeight.w700,  fontSize: 13),),
                                      ),
                                      Container(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 5, horizontal: 10),
                                        child: Text("172,631.94 COP", style: TextStyle(color: Colors.black45),),
                                      ),
                                    ],
                                  ),
                                  TableRow(
                                    children: <Widget>[
                                      Container(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 5, horizontal: 10),
                                        child: const Text("TRM", style: TextStyle(color: Colors.black45, fontWeight: FontWeight.w700,  fontSize: 13),),
                                      ),
                                      Container(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 5, horizontal: 10),
                                        child: Text("4,709.00 COP", style: TextStyle(color: Colors.black45),),
                                      ),
                                    ],
                                  ),
                                  TableRow(
                                    children: <Widget>[
                                      Container(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 5, horizontal: 10),
                                        child: const Text("TOTAL PAGADO", style: TextStyle(color: Colors.black45, fontWeight: FontWeight.w700,  fontSize: 13),),
                                      ),
                                      Container(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 5, horizontal: 10),
                                        child: Text("\$3.256.246,41 COP", style: TextStyle(color:  const Color(0xff6C4F92)),),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                              const SizedBox(height: 20),
                            ],
                          ),
                        )),
                    Positioned(
                      top: 350,
                      right: 2,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Material(
                            type: MaterialType.transparency,
                            child: Ink(
                              decoration: BoxDecoration(
                                  border: Border.all(
                                      color: const Color(0xff6C4F92), width: 2),
                                  color: const Color(0xff6C4F92),
                                  borderRadius: BorderRadius.circular(50.0)),
                              //<-- SEE HERE
                              child: InkWell(
                                borderRadius: BorderRadius.circular(100.0),
                                onTap: () {
                                  Navigator.pushNamed(context, 'preview_screen_invoice');
                                },
                                child: const Padding(
                                  padding: EdgeInsets.all(10.0),
                                  child: Icon(
                                    Icons.download,
                                    size: 35.0,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          const SizedBox(width: 5),
                        ],
                      ),
                    ),
                    const SizedBox(height: 290),
                  ]),
            ),
            const SizedBox(height: 100),
          ],
        ),
      ),
    ));
  }
}

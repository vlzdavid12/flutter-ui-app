import 'package:flutter/material.dart';
import 'package:xisfo_app/helpers/helpers.dart';

class AlertWalletError extends StatelessWidget {
  final String titleAlert;
  final String descriptionAlert;
  final String txtBtn;
  final VoidCallback onTap;

  const AlertWalletError(
      {Key? key,
      required this.titleAlert,
      required this.descriptionAlert,
      required this.txtBtn,
      required this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    changeStatusLight();
    final size = MediaQuery.of(context).size;

    return Scaffold(
        body: SafeArea(
      child:  Center(
        child: Container(
        width: 550,
        decoration: const BoxDecoration(
        image: DecorationImage(
        alignment: Alignment.center,
        image: AssetImage("assets/images/fondo_total.png"),
    fit: BoxFit.cover),
    ),
    padding: const EdgeInsets.symmetric(horizontal: 15),
    child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              children: [
                const SizedBox(height: 90),
                Center(
                    child: Image.asset('assets/images/logo-purple.png',
                        width: 130)),
                const SizedBox(height: 20),
                SizedBox(
                  width: 340,
                  child: Center(
                    child: Text(titleAlert,
                        style: const TextStyle(
                            fontSize: 44,
                            fontFamily: 'Dongle',
                            height: 0.8,
                            fontWeight: FontWeight.w700,
                            color: Color(0xff6C4F92)),
                        textAlign: TextAlign.center),
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                SizedBox(
                  width: 300,
                  child:  Text(descriptionAlert, textAlign: TextAlign.center),
                ),
                const SizedBox(
                  height: 20,
                ),
                MaterialButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(50)),
                    disabledColor: const Color(0xffc7a314),
                    focusColor: const Color(0xffc7a314),
                    splashColor: const Color(0xffc7a314),
                    highlightColor: const Color(0xffEDCF53),
                    elevation: 0,
                    color: const Color(0xffEDCF53),
                    onPressed: onTap,
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 40, vertical: 15),
                      child: Text(txtBtn.toUpperCase(),
                          style: const TextStyle(
                              color: Color(0xff6C4F92),
                              fontWeight: FontWeight.w700)),
                    )),
              ],
            ),
            Image.asset(
              'assets/images/persons/person_error_wallet.png',
              width: size.width / 1.7,
            )
          ]),
    ))));
  }
}

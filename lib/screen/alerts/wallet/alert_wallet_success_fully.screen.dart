import 'package:flutter/material.dart';
import 'package:xisfo_app/models/list_options.model.dart';
import 'package:xisfo_app/helpers/helpers.dart';

class AlertWalletSuccessFully extends StatelessWidget {
  final String titleAlert;
  final String txtBtn;
  final VoidCallback onTap;

  const AlertWalletSuccessFully(
      {Key? key,
      required this.titleAlert,
      required this.txtBtn,
      required this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    changeStatusLight();
    final size = MediaQuery.of(context).size;
    List<Options> options = [
      const Options(title: 'Monetiza'),
      const Options(title: 'Recibe'),
      const Options(title: 'Paga'),
      const Options(title: 'Cashback'),
      const Options(title: 'Créditos')
    ];
    return Scaffold(
        body: SafeArea(
            child: SizedBox(
      width: double.infinity, //Your desire Width
      height: double.infinity, //Your desire Height
      child: Center(
          child: Container(
            width: 550,
            decoration: const BoxDecoration(
              image: DecorationImage(
                  alignment: Alignment.center,
                  image: AssetImage("assets/images/fondo_total.png"),
                  fit: BoxFit.cover),
            ),
            padding: const EdgeInsets.symmetric(horizontal: 15),
            child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Column(
            children: [
              const SizedBox(height: 80),
              Image.asset('assets/images/logo-purple.png', width: 130),
              const SizedBox(height: 20),
              const SizedBox(
                width: 340,
                child: Center(
                  child: Text('¡Has creado tu billetera Xisfo exitosamente!',
                      style: TextStyle(fontSize: 44,   fontFamily: 'Dongle',  height: 0.8, fontWeight: FontWeight.w700, color: Color(0xff6C4F92)),
                      textAlign: TextAlign.center),
                ),
              ),
              const SizedBox(height: 10),
              SizedBox(
                width: 280,
                height: size.height / 9.5,
                child: Wrap(
                  alignment: WrapAlignment.center,
                  children: <Widget>[
                    ...options.map((option) {
                      return Container(
                        padding: const EdgeInsets.all(10),
                        child: Text(option.title, textAlign: TextAlign.center, style: const TextStyle(color: Colors.black45, fontSize: 16)),
                      );
                    }),
                  ],
                ),
              ),
              MaterialButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(50)),
                  disabledColor: const Color(0xffc7a314),
                  focusColor: const Color(0xffc7a314),
                  splashColor: const Color(0xffc7a314),
                  highlightColor: const Color(0xffEDCF53),
                  elevation: 0,
                  color: const Color(0xffEDCF53),
                  onPressed: onTap,
                  child: Container(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 40, vertical: 15),
                    child: Text(txtBtn.toUpperCase(),
                        style: const TextStyle(
                            color: Color(0xff6C4F92),
                            fontWeight: FontWeight.w700)),
                  )),
            ],
          ),
          Image.asset(
            'assets/images/persons/person_success_fully_wallet.png',
            width: size.width / 2.2,
          )
        ],
      ),
    )))));
  }
}

import 'dart:math';
import 'package:flutter/material.dart';
import 'package:xisfo_app/helpers/helpers.dart';

class GeneralMoneySuccessAlertScreen extends StatelessWidget {
  final String titleAlert;
  final String descriptionAlert;
  final String txtBtn;
  final VoidCallback onTap;
  const GeneralMoneySuccessAlertScreen({Key? key, required this.titleAlert, required this.descriptionAlert, required this.onTap, required this.txtBtn}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final numberImage = Random().nextInt(2) + 1;
    changeStatusLight();
    return Scaffold(
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Center( child: Image.asset('assets/images/logo-purple.png', width: 130)),
            SizedBox(
              width: 300,
              child: Column(
                children: [
                  const SizedBox(height: 20),
                  Text(titleAlert, style: const TextStyle(
                      color: Color(0xff6C4F92),
                      fontSize: 45,
                      fontWeight: FontWeight.w700,
                      fontFamily: 'Dongle',
                      letterSpacing: -2,
                      height: 0.7
                  ), textAlign: TextAlign.center),
                  const SizedBox(height: 10),
                  Text(descriptionAlert, style: const TextStyle(color: Colors.black45, height: 1.5), textAlign: TextAlign.center),
                ],
              ),
            ),
            const SizedBox(height: 30),
            MaterialButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(50)),
                disabledColor: const Color(0xffc7a314),
                focusColor: const Color(0xffc7a314),
                splashColor: const Color(0xffc7a314),
                highlightColor: const Color(0xffEDCF53),
                elevation: 0,
                color: const Color(0xffEDCF53),
                onPressed: onTap,
                child: Container(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 40, vertical: 15),
                  child: Text(txtBtn.toUpperCase(),
                      style: const TextStyle(
                          color: Color(0xff6C4F92),
                          fontWeight: FontWeight.w700)),
                )),
            Image.asset('assets/images/alert/alert_money_person_$numberImage.png', width: 240)
          ],
        ),
      )
    );
  }
}

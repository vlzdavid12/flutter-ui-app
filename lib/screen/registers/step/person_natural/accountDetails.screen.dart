import 'package:flutter/material.dart';
import 'package:xisfo_app/widgets/btnGroup.widget.dart';
import 'package:xisfo_app/generated/l10n.dart';
import 'package:xisfo_app/ui/input_decorations.ui.dart';
import 'package:xisfo_app/widgets/form/dropdown_input.widgets.dart';
import 'package:xisfo_app/widgets/header.widget.dart';

class StepOne extends StatefulWidget {
  PageController pageController;
  S internalization;
  int pageChanged;


  StepOne(this.pageController, this.pageChanged, this.internalization);

  @override
  State<StepOne> createState() => _StepOneState();
}

class _StepOneState extends State<StepOne> {
  TextEditingController dropDownSelect = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final AutovalidateMode _autoValidateMode = AutovalidateMode.always;
  @override
  Widget build(BuildContext context) {
    final internalization = S.of(context);

    return SingleChildScrollView(
      child: Container(
        margin: const EdgeInsets.all(0),
        color: Colors.transparent,
        child: Form(
          key: _formKey,
          autovalidateMode: _autoValidateMode,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              HeaderFormWidget(
                stepNumber: '01',
                title: internalization.account,
                subTitle: internalization.detailsAccount,
              ),
              TextFormField(
                enableSuggestions: false,
                cursorColor: Colors.grey,
                autocorrect: false,
                style: const TextStyle(color: Colors.black45),
                keyboardType: TextInputType.phone,
                decoration: InputDecorations.generalInputDecoration(
                    hinText: '000 000 0000',
                    labelText: internalization.numberMobil,
                    prefixIcon: Icons.numbers,
                    colorInput: Colors.grey,
                    colorError: const Color(0xFFC21839)
                ),
                textInputAction: TextInputAction.next,
                onEditingComplete: () => FocusScope.of(context).nextFocus(),
                validator: (String? value) {
                  if (value == null || value.trim().isEmpty) {
                    return "Ingresa el número de celular.";
                  }
                  return null;
                },
              ),
              const SizedBox(height: 8),
              TextFormField(
                maxLength: 4,
                enableSuggestions: false,
                cursorColor: Colors.grey,
                autocorrect: false,
                style: const TextStyle(color: Colors.black45),
                keyboardType: TextInputType.phone,
                obscureText: true,
                decoration: InputDecorations.generalInputDecoration(
                    hinText: '****',
                    labelText: internalization.createPin,
                    prefixIcon: Icons.password,
                    colorInput: Colors.grey,
                    colorError: const Color(0xFFC21839)
                ),
                textInputAction: TextInputAction.next,
                onEditingComplete: () => FocusScope.of(context).nextFocus(),
                validator: (String? value){
                  if(value == null || value.trim().isEmpty){
                    return "Ingresa el PIN.";
                  }
                  return null;
                },
              ),
              const SizedBox(height: 8),
              TextFormField(
                enableSuggestions: false,
                cursorColor: Colors.grey,
                autocorrect: false,
                style: const TextStyle(color: Colors.black45),
                keyboardType: TextInputType.text,
                decoration: InputDecorations.generalInputDecoration(
                    hinText: '',
                    labelText: 'Nombres',
                    prefixIcon: Icons.account_circle,
                    colorInput: Colors.grey,
                    colorError: const Color(0xFFC21839)),
                textInputAction: TextInputAction.next,
                onEditingComplete: () => FocusScope.of(context).nextFocus(),
                validator: (String? value){
                  if(value == null || value.trim().isEmpty){
                    return "Ingresa el nombre completo.";
                  }
                  return null;
                },
              ),
              const SizedBox(height: 8),
              TextFormField(
                enableSuggestions: false,
                cursorColor: Colors.grey,
                autocorrect: false,
                style: const TextStyle(color: Colors.black45),
                keyboardType: TextInputType.text,
                decoration: InputDecorations.generalInputDecoration(
                    hinText: '',
                    labelText: 'Apellidos',
                    prefixIcon: Icons.account_circle,
                    colorInput: Colors.grey,
                    colorError: const Color(0xFFC21839)
                ),
                textInputAction: TextInputAction.next,
                onEditingComplete: () => FocusScope.of(context).nextFocus(),
                validator: (String? value){
                  if(value == null || value.trim().isEmpty){
                    return "Ingresa el apellido completo.";
                  }
                  return null;
                },
              ),
              const SizedBox(height: 8),
              DopDownInput(list: [''], labelDocument: 'Tipo de documento', dropDownSelect: dropDownSelect, prefixIcon: Icons.description,),
              const SizedBox(height: 8),
              TextFormField(
                enableSuggestions: false,
                cursorColor: Colors.grey,
                autocorrect: false,
                style: const TextStyle(color: Colors.black45),
                keyboardType: TextInputType.text,
                decoration: InputDecorations.generalInputDecoration(
                    hinText: '0123456789',
                    labelText: internalization.noIdentification,
                    prefixIcon: Icons.numbers,
                    colorInput: Colors.grey,
                    colorError: const Color(0xFFC21839)),
                textInputAction: TextInputAction.next,
                onEditingComplete: () => FocusScope.of(context).nextFocus(),
                validator: (String? value){
                  if(value == null || value.trim().isEmpty){
                    return "Ingresa el número de identificación.";
                  }
                  return null;
                },
              ),
              const SizedBox(height: 8),
              TextFormField(
                enableSuggestions: false,
                cursorColor: Colors.grey,
                autocorrect: false,
                style: const TextStyle(color: Colors.black45),
                keyboardType: TextInputType.emailAddress,
                decoration: InputDecorations.generalInputDecoration(
                    hinText: 'micorreo@xisfo.co',
                    labelText: internalization.email,
                    prefixIcon: Icons.email,
                    colorInput: Colors.grey,
                    colorError: const Color(0xFFC21839)
                ),
                textInputAction: TextInputAction.next,
                onEditingComplete: () => FocusScope.of(context).nextFocus(),
                validator: (String? value){
                  if(value == null || value.trim().isEmpty){
                    return "Ingresa el correo electrónico.";
                  }
                  return null;
                },
              ),

              const SizedBox(
                height: 10,
              ),
              BtnGroupWidget(pageController: widget.pageController, pageChanged: widget.pageChanged, internalization: internalization, formKey: _formKey)
            ],
          ),
        ),
      ),
    );
  }
}

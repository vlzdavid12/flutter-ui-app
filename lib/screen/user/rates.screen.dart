import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';

import '../../generated/l10n.dart';
import '../../widgets/layout.widgets.dart';
class RatesScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final international = S.of(context);
    return LayoutApp(
      children: SingleChildScrollView(
      child: Column(
      children: [
        SizedBox(
          width: 290,
          child: Text(international.transferKnow("Cafer"),
            style: const TextStyle(
                color: Color(0xff6C4F92),
                fontSize: 42,
                fontFamily: 'Dongle',
                height: 0.8),
          ),
        ),
        Container(
          width: 300,
          margin: const EdgeInsets.symmetric(vertical: 4),
          child: DottedBorder(
            padding:
            const EdgeInsets.symmetric(vertical: 20, horizontal: 10),
            borderType: BorderType.RRect,
            radius: const Radius.circular(10),
            dashPattern: const [4, 4],
            strokeWidth: 2,
            color: const Color(0xff6C4F92),
            child: Column(
              children: const [
                Text("5%",
                    style: TextStyle(
                        color: Colors.black54,
                        fontWeight: FontWeight.w700,
                        fontSize: 24,
                        height: 1.0)),
                Text("Por cada transacción",
                    style: TextStyle(
                        color: Colors.black54, fontSize: 16, height: 2.0)),
                Text("SERVICIO DE MONEDA EXTRANJERA",
                    style: TextStyle(
                        color: Colors.black54, fontSize: 14, height: 1.5))
              ],
            ),
          ),
        ),
        Container(
            width: 300,
            margin: const EdgeInsets.symmetric(vertical: 4),
            child: DottedBorder(
              padding:
              const EdgeInsets.symmetric(vertical: 20, horizontal: 10),
              borderType: BorderType.RRect,
              radius: const Radius.circular(10),
              dashPattern: const [4, 4],
              strokeWidth: 2,
              color: const Color(0xff6C4F92),
              child: Column(
                children: const [
                  Text("5 USD",
                      style: TextStyle(
                          color: Colors.black54,
                          fontWeight: FontWeight.w700,
                          fontSize: 24,
                          height: 1.0)),
                  Text("Por cada transacción",
                      style: TextStyle(
                          color: Colors.black54,
                          fontSize: 16,
                          height: 2.0)),
                  Text(
                    "COSTO TRANSFERENCIA INTERNACIONAL",
                    style: TextStyle(
                        color: Colors.black54, fontSize: 14, height: 1.5),
                    textAlign: TextAlign.center,
                  )
                ],
              ),
            )),
        Container(
            width: 300,
            margin: const EdgeInsets.symmetric(vertical: 4),
            child: DottedBorder(
              padding:
              const EdgeInsets.symmetric(vertical: 20, horizontal: 10),
              borderType: BorderType.RRect,
              radius: const Radius.circular(10),
              dashPattern: const [4, 4],
              strokeWidth: 2,
              color: const Color(0xff6C4F92),
              child: Column(
                children: const [
                  Center(
                      child: Text(
                        "10 USD + 0,005 ",
                        style: TextStyle(
                            color: Colors.black54,
                            fontWeight: FontWeight.w700,
                            fontSize: 24,
                            height: 1.0),
                        textAlign: TextAlign.center,
                      )),
                  Center(
                      child: Text("(por cada dolar)",
                          style: TextStyle(
                              color: Colors.black54,
                              fontSize: 16,
                              height: 2.0))),
                  Center(
                      child: Text("COSTO EPAY SERVICES",
                          style: TextStyle(
                              color: Colors.black54,
                              fontSize: 14,
                              height: 1.5)))
                ],
              ),
            )),
        Container(
            width: 300,
            margin: const EdgeInsets.symmetric(vertical: 4),
            child: DottedBorder(
              padding:
              const EdgeInsets.symmetric(vertical: 20, horizontal: 10),
              borderType: BorderType.RRect,
              radius: const Radius.circular(10),
              dashPattern: const [4, 4],
              strokeWidth: 2,
              color: const Color(0xff6C4F92),
              child: Column(
                children: const [
                  Center(
                      child: Text("30 USD + 0,005",
                          style: TextStyle(
                              color: Colors.black54,
                              fontWeight: FontWeight.w700,
                              fontSize: 24,
                              height: 1.0))),
                  Center(
                    child: Text("(por cada dolar)",
                        style: TextStyle(
                            color: Colors.black54,
                            fontSize: 16,
                            height: 2.0)),
                  ),
                  Center(
                    child: Text("COSTO EPAY SERVICES EXPRESS",
                        style: TextStyle(
                            color: Colors.black54,
                            fontSize: 14,
                            height: 1.5)),
                  )
                ],
              ),
            )),
        Container(
            width: 300,
            margin: const EdgeInsets.symmetric(vertical: 4),
            child: DottedBorder(
              padding:
              const EdgeInsets.symmetric(vertical: 20, horizontal: 10),
              borderType: BorderType.RRect,
              radius: const Radius.circular(10),
              dashPattern: const [4, 4],
              strokeWidth: 2,
              color: const Color(0xff6C4F92),
              child: Column(
                children: const [
                  Center(
                      child: Text("2 USD",
                          style: TextStyle(
                              color: Colors.black54,
                              fontWeight: FontWeight.w700,
                              fontSize: 24,
                              height: 1.0))),
                  Center(
                    child: Text(" por cada carga semanal",
                        style: TextStyle(
                            color: Colors.black54,
                            fontSize: 16,
                            height: 2.0)),
                  ),
                  Center(
                    child: Text("COSTO STREAMATE",
                        style: TextStyle(
                            color: Colors.black54,
                            fontSize: 14,
                            height: 1.5)),
                  )
                ],
              ),
            )),
        const SizedBox(
          height: 120,
        )
      ],
    )));
  }
}

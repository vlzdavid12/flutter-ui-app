import 'package:flutter/material.dart';
import 'package:xisfo_app/widgets/layout.widgets.dart';
import 'package:xisfo_app/models/models.dart';

import '../../generated/l10n.dart';

class HelpScreen extends StatefulWidget {
  @override
  State<HelpScreen> createState() => _HelpScreenState();
}

class _HelpScreenState extends State<HelpScreen> {
  int _tabPosition = 1;

  final List<ItemAccordion> _data = [
    ItemAccordion(
        headerValue: '¿Cómo puedo abrir mi cuenta en mi App Xisfo?',
        expandedValue:
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.'),
    ItemAccordion(
        headerValue: '¿Puedo hacer retiros directamente desde mi App Xisfo?',
        expandedValue:
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.'),
    ItemAccordion(
        headerValue: '¿Puedo enviar dinero a cualquier banco?',
        expandedValue:
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.'),
    ItemAccordion(
        headerValue: '¿Qué puedo pagar desde mi App Xisfo?',
        expandedValue:
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.'),
    ItemAccordion(
        headerValue: '¿Tiene algún costo utilizar mi App Xisfo?',
        expandedValue:
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.')
  ];

  @override
  Widget build(BuildContext context) {
    final internalization =  S.of(context);

    final List<TabItem> tabs = [
      TabItem(
        icon: Image.asset("assets/images/menu/Wallet.png", width: 30),
        title: internalization.yourWallet,
        action: 1,
      ),
      TabItem(
        icon: Image.asset("assets/images/menu/Money.png", width: 30),
        title: internalization.foreignCurrencyMoney,
        action: 2,
      ),
    ];
    return LayoutApp(
        children: Padding(
            padding: const EdgeInsets.all(15),
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(internalization.helpTitle,
                      style: const TextStyle(
                          fontSize: 45,
                          fontFamily: 'Dongle',
                          fontWeight: FontWeight.w700,
                          height: 0.8,
                          color: Color(0xff6C4F92))),
                  SizedBox(
                      width: 320,
                      height: 120,
                      child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: tabs.length,
                          itemBuilder: (BuildContext context, int i) {
                            return _itemsTabs(
                                tabs[i].icon, tabs[i].title, tabs[i].action);
                          })),
                  const SizedBox(height: 10),
                  _switchCase(internalization),
                ],
              ),
            )));
  }

  Widget _switchCase(internalization) {
    switch (_tabPosition) {
      case 2:
        return Column(
          children: [
            Text(
              internalization.subHelpTitle,
              style: const TextStyle(color: Colors.black45, fontSize: 14),
              textAlign: TextAlign.center,
            ),
            const SizedBox(height: 10),
            ExpansionPanelList(
              elevation: 0,
              dividerColor: Colors.transparent,
              expansionCallback: (int index, bool isExpanded) {
                setState(() {
                  _data[index].isExpanded = !isExpanded;
                });
              },
              children: _data.map<ExpansionPanel>((ItemAccordion item) {
                return ExpansionPanel(
                    backgroundColor: Colors.transparent,
                    headerBuilder: (BuildContext context, bool Expanded) {
                      return ListTile(
                        horizontalTitleGap: 0,
                        minVerticalPadding: 0,
                        title: Text(item.headerValue),
                      );
                    },
                    body: ListTile(
                      horizontalTitleGap: 0,
                      minVerticalPadding: 0,
                      title: Text(item.expandedValue,
                          style: const TextStyle(fontSize: 14, height: 1.4),
                          textAlign: TextAlign.justify),
                      onTap: () {
                        setState(() {
                          _data.removeWhere((ItemAccordion currentItem) =>
                              item == currentItem);
                        });
                      },
                    ),
                    isExpanded: item.isExpanded);
              }).toList(),
            ),
            const SizedBox(height: 100),
          ],
        );
      default:
        return Column(
          children: [
            Text(
              internalization.subHelpTitle,
              style: const TextStyle(color: Colors.black45, fontSize: 14),
              textAlign: TextAlign.center,
            ),
            const SizedBox(height: 10),
            ExpansionPanelList(
              elevation: 0,
              dividerColor: Colors.transparent,
              expansionCallback: (int index, bool isExpanded) {
                setState(() {
                  _data[index].isExpanded = !isExpanded;
                });
              },
              children: _data.map<ExpansionPanel>((ItemAccordion item) {
                return ExpansionPanel(
                    backgroundColor: Colors.transparent,
                    headerBuilder: (BuildContext context, bool Expanded) {
                      return ListTile(
                        horizontalTitleGap: 0,
                        minVerticalPadding: 0,
                        title: Text(item.headerValue),
                      );
                    },
                    body: ListTile(
                      horizontalTitleGap: 0,
                      minVerticalPadding: 0,
                      title: Text(item.expandedValue,
                          style: const TextStyle(fontSize: 14, height: 1.4),
                          textAlign: TextAlign.justify),
                      onTap: () {
                        setState(() {
                          _data.removeWhere((ItemAccordion currentItem) =>
                              item == currentItem);
                        });
                      },
                    ),
                    isExpanded: item.isExpanded);
              }).toList(),
            ),
            const SizedBox(height: 100),
          ],
        );
    }
  }

  Widget _itemsTabs(Widget icon, String title, int action) {
    return GestureDetector(
      onTap: () {
        setState(() {
          _tabPosition = action;
        });
      },
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: const Color(0xff6C4F92),
        ),
        width: 145,
        margin: const EdgeInsets.symmetric(horizontal: 4, vertical: 4),
        padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 5),
        child: Column(
          children: [
            icon,
            const SizedBox(
              height: 10,
            ),
            Text(
              title,
              style: const TextStyle(color: Colors.white, fontSize: 12),
              textAlign: TextAlign.center,
            )
          ],
        ),
      ),
    );
  }
}

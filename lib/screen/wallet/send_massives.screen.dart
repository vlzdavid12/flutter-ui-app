import "package:flutter/material.dart";
import 'package:xisfo_app/widgets/form/upload_input.widgets.dart';
import 'package:xisfo_app/widgets/layout.widgets.dart';

import 'package:xisfo_app/generated/l10n.dart';

class SendMassiveScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final internalization = S.of(context);
    return LayoutApp(
        children: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(internalization.notCompliedPayment('Cafer'), style: const TextStyle(fontSize: 45, fontFamily: 'Dongle', height: 0.6)),
                const SizedBox(height: 10),
                Text(internalization.descriptionThinkClick, style: const TextStyle(fontSize: 14, color: Colors.black54), textAlign: TextAlign.justify),
                const SizedBox(height: 10),
                Text( internalization.noteDispersion("2.000"), style: const TextStyle(fontSize: 12), textAlign: TextAlign.left),
                const SizedBox(height: 6),
                Text(internalization.downLoadTemplate, style: const TextStyle(fontSize: 34, fontFamily: 'dongle')),
                const SizedBox(height: 6),
                MaterialButton(
                    minWidth:320,
                    height: 50,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    disabledColor: const Color(0xffc7a314),
                    focusColor: const Color(0xffc7a314),
                    splashColor: const Color(0xffc7a314),
                    highlightColor: const Color(0xffEDCF53),
                    elevation: 0,
                    color: const Color(0xffEDCF53),
                    onPressed: () {},
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children:  [
                        const Icon(Icons.download_rounded, color: Color(0xff6C4F92)),
                        const SizedBox(width: 20.0),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(internalization.downloadFormat,
                                style: const TextStyle(
                                    color: Color(0xff6C4F92),
                                    fontWeight: FontWeight.w700,
                                    height: 1.4)),
                            Text(internalization.massiveShipments,
                                style: const TextStyle(
                                    fontSize: 12,
                                    color: Color(0xff6C4F92),
                                    height: 1.0)),
                          ],
                        )
                      ],
                    )),
                const SizedBox(height: 6),
                Text(internalization.addArchive, style: const TextStyle(fontSize: 34, fontFamily: 'dongle')),
                const SizedBox(height: 6),
                UploadInput(),
                const SizedBox(height: 20),
                MaterialButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    disabledColor: const Color(0xffa18bc7),
                    focusColor: const Color(0xff4d2d79),
                    splashColor: const Color(0xff53298a),
                    highlightColor: const Color(0xff6C4F92),
                    elevation: 0,
                    color: const Color(0xff6C4F92),
                    child: Container(
                      width: double.infinity,
                      padding: const EdgeInsets.symmetric(vertical: 15),
                      child: Text(
                       internalization.sendPaymentsMassive,
                        style: const TextStyle(
                            color: Colors.white, fontWeight: FontWeight.w700),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    onPressed: () {}),
                const SizedBox(height: 100),
              ],
            ),
          ),
        )
    );
  }
}

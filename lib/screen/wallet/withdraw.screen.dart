import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:xisfo_app/widgets/widgets.dart';
import 'package:xisfo_app/generated/l10n.dart';
import 'package:xisfo_app/ui/input_decorations.ui.dart';

const List<String> list = ['Bancolombia'];

class WithDrawScreen extends StatelessWidget {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    TextEditingController dropDownSelect = TextEditingController();
    final international = S.of(context);

    return LayoutApp(
        children: SingleChildScrollView(
      child: Padding(
          padding: const EdgeInsets.all(15),
          child: Form(
            key: _formKey,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                const Text('¿Quieres retirar, Cafer? ',
                    style: TextStyle(
                        fontSize: 45,
                        fontFamily: 'Dongle',
                        fontWeight: FontWeight.w700,
                        color: Color(0xff6C4F92))),
                _TotalWallet(),
                const SizedBox(
                  height: 20,
                ),
                Container(
                  margin: const EdgeInsets.symmetric(vertical: 8),
                  width: 550,
                  child: TextFormField(
                    autocorrect: false,
                    style: const TextStyle(color: Colors.grey),
                    keyboardType: TextInputType.number,
                    decoration: InputDecorations.generalInputDecoration(
                        hinText: '0.00',
                        labelText: 'Monto',
                        prefixIcon: Icons.attach_money,
                        colorInput: Colors.black45,
                        colorError: const Color(0xFFC21839)),
                  ),
                ),
                DopDownInput(
                  list: list,
                  labelDocument: "Seleccione Banco",
                  dropDownSelect: dropDownSelect,
                  prefixIcon: Icons.business,
                ),
                CheckBoxInput(lisTitles: const [
                  'Confirmo que deseo retirar de mi ',
                  'billetera a la cuenta bancaria',
                  'seleccionada'
                ]),
                Center(
                  child: MaterialButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      disabledColor: const Color(0xffc7a314),
                      focusColor: const Color(0xffc7a314),
                      splashColor: const Color(0xffc7a314),
                      highlightColor: const Color(0xffEDCF53),
                      elevation: 0,
                      color: const Color(0xffEDCF53),
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 60, vertical: 15),
                        child: Text(international.withDrawMount.toUpperCase(),
                            style: const TextStyle(
                                color: Color(0xff6C4F92),
                                fontWeight: FontWeight.w700)),
                      ),
                      onPressed: () {}),
                ),
                const SizedBox(
                  height: 20,
                ),
                Center(
                  child: MaterialButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      disabledColor: const Color(0xffa18bc7),
                      focusColor: const Color(0xff4d2d79),
                      splashColor: const Color(0xff53298a),
                      highlightColor: const Color(0xff6C4F92),
                      elevation: 0,
                      color: const Color(0xff6C4F92),
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 22, vertical: 15),
                        child: Text(international.addAccountBank.toUpperCase(),
                            style: const TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w700)),
                      ),
                      onPressed: () => Navigator.pushNamed(context, 'bank')),
                ),
                const SizedBox(
                  height: 150,
                ),
              ],
            ),
          )),
    ));
  }
}

class _TotalWallet extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        FadeInLeft(
          child: Container(
            width: 150,
            height: 180,
            decoration: const BoxDecoration(
                color: Color(0xFFEDCF53),
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(0),
                    topRight: Radius.circular(20),
                    bottomLeft: Radius.circular(20),
                    bottomRight: Radius.circular(20))),
            child: null,
          ),
        ),
        FadeInDown(
          child: Container(
            margin: const EdgeInsets.only(top: 55, left: 10),
            width: double.infinity,
            padding:
                const EdgeInsets.only(top: 25, left: 10, right: 20, bottom: 10),
            decoration: BoxDecoration(
                color: const Color(0xff6C4F92),
                borderRadius: BorderRadius.circular(40)),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                      padding: const EdgeInsets.all(20),
                      width: 70,
                      child:
                          Image.asset('assets/images/icons/wallet.icon.png')),
                  SizedBox(
                    width: 180,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text("Disponible",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 35,
                                fontFamily: 'Dongle',
                                height: 0.9)),
                        SizedBox(
                          width: 180,
                          child:
                              Flex(direction: Axis.horizontal, children: const [
                            Expanded(
                                flex: 1,
                                child: Text("\$300.000.00",
                                    softWrap: false,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 41,
                                        fontFamily: 'Dongle',
                                        height: 0.8))),
                          ]),
                        ),
                      ],
                    ),
                  ),
                ]),
          ),
        )
      ],
    );
  }
}

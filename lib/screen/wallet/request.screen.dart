import 'package:flutter/material.dart';
import 'package:xisfo_app/screen/alerts/transaccion/alert_trasaccion_error.screen.dart';
import 'package:xisfo_app/widgets/layout.widgets.dart';

import 'package:xisfo_app/generated/l10n.dart';
import 'package:xisfo_app/ui/input_decorations.ui.dart';

class RequestScreen extends StatelessWidget {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    final internalization = S.of(context);
    return LayoutApp(
        children: Padding(
      padding: const EdgeInsets.all(15),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Center(
              child: Container(
                margin: const EdgeInsets.only(bottom: 20),
                width: 177,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(50),
                    color: Colors.black12),
                child: Row(
                  children: [
                    Container(
                        height: 39,
                        width: 90,
                        padding: const EdgeInsets.only(
                          left: 20,
                          right: 20,
                          top: 10,
                        ),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(50),
                            color: const Color(0xff6C4F92)),
                        child: Text(
                          internalization.asks,
                          style: const TextStyle(color: Colors.white),
                          textAlign: TextAlign.center,
                        )),
                    Container(
                        height: 40,
                        padding: const EdgeInsets.only(left: 5),
                        child: TextButton(
                          onPressed: () {
                            Navigator.pushNamed(context, 'send');
                          },
                          child: Text(internalization.send,
                              style: const TextStyle(
                                  color: Colors.black45,
                                  fontWeight: FontWeight.w700),
                              textAlign: TextAlign.center),
                        )),
                  ],
                ),
              ),
            ),
            Form(
              key: _formKey,
              autovalidateMode: AutovalidateMode.always,
              child: Column(children: [
                Text(internalization.asksTitle("Cafer"),
                    style: const TextStyle(
                        color: Color(0xff6C4F92),
                        fontFamily: 'Dongle',
                        fontSize: 40,
                        fontWeight: FontWeight.w700,
                        height: 0.8)),
                const SizedBox(
                  height: 20,
                ),
                _SliderListFavorites(),
                Text(internalization.asksFriends,
                    style: const TextStyle(
                        color: Colors.black,
                        fontSize: 18,
                        fontWeight: FontWeight.w700)),
                const SizedBox(
                  height: 20,
                ),
                Container(
                  margin: const EdgeInsets.symmetric(vertical: 8),
                  width: 550,
                  child: TextFormField(
                    autocorrect: false,
                    style: const TextStyle(color: Colors.grey),
                    keyboardType: TextInputType.number,
                    textInputAction: TextInputAction.next,
                    onEditingComplete: () => FocusScope.of(context).nextFocus(),
                    decoration: InputDecorations.generalInputDecoration(
                        hinText: '000 000 0000',
                        labelText: 'Ingresa el número celular',
                        prefixIcon: Icons.numbers_sharp,
                        colorInput: Colors.black45,
                        colorError: const Color(0xFFC21839)),
                    validator: (String? value) {
                      if (value == null || value.trim().isEmpty) {
                        return "El valor o monto es requerido";
                      }
                    },
                  ),
                ),
                Container(
                  margin: const EdgeInsets.symmetric(vertical: 8),
                  width: 550,
                  child: TextFormField(
                    autocorrect: false,
                    style: const TextStyle(color: Colors.grey),
                    keyboardType: TextInputType.number,
                    textInputAction: TextInputAction.next,
                    onEditingComplete: () => FocusScope.of(context).nextFocus(),
                    decoration: InputDecorations.generalInputDecoration(
                        hinText: '0.00',
                        labelText: internalization.valueTotal,
                        prefixIcon: Icons.attach_money,
                        colorInput: Colors.black45,
                        colorError: const Color(0xFFC21839)),
                    validator: (String? value) {
                      if (value == null || value.trim().isEmpty) {
                        return "El valor o monto es requerido";
                      }
                    },
                  ),
                ),
                Center(
                  child: MaterialButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      disabledColor: const Color(0xffc7a314),
                      focusColor: const Color(0xffc7a314),
                      splashColor: const Color(0xffc7a314),
                      highlightColor: const Color(0xffEDCF53),
                      elevation: 0,
                      color: const Color(0xffEDCF53),
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 80, vertical: 15),
                        child: Text(internalization.asks.toUpperCase(),
                            style: const TextStyle(
                                color: Color(0xff6C4F92),
                                fontWeight: FontWeight.w700)),
                      ),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => AlertTrasactionError(
                                    title: '¡Transacción Rechazada!',
                                    description:
                                        'Tu transacción falló, tienes fondos insuficientes para realizar esta operación',
                                    onTap: () {},
                                    txtBtn: 'Verificar',
                                  )),
                        );
                      }),
                ),
                const SizedBox(
                  height: 130,
                ),
              ]),
            )
          ],
        ),
      ),
    ));
  }
}

class _SliderListFavorites extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final international = S.of(context);
    return SingleChildScrollView(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
          Text(international.textFavorite,
              style: const TextStyle(
                  color: Colors.black,
                  fontSize: 18,
                  fontWeight: FontWeight.w700)),
          Container(
            margin: const EdgeInsets.symmetric(vertical: 10),
            width: 550,
            height: 140,
            child: ListView.builder(
              itemCount: 10,
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, index) => _BoxFavorites(),
            ),
          ),
        ]));
  }
}

class _BoxFavorites extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 75,
      width: 75,
      margin: const EdgeInsets.all(10),
      child: Padding(
        padding: const EdgeInsets.all(5),
        child: _ItemFavorite(),
      ),
    );
  }
}

class _ItemFavorite extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: const [
        CircleAvatar(
          backgroundColor: Colors.grey,
          radius: 30.0,
          foregroundImage: AssetImage('assets/images/image-no-found.png'),
          child: null,
        ),
        SizedBox(height: 10),
        Text(
          'Camilo Torres',
          style: TextStyle(fontSize: 12, color: Colors.black45),
          textAlign: TextAlign.center,
        ),
      ],
    );
  }
}

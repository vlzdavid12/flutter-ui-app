import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_offline/flutter_offline.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flutter/services.dart';
import 'package:page_transition/page_transition.dart';
import 'package:xisfo_app/bloc/auth/step/step_bloc.dart';
import 'package:xisfo_app/bloc/foreign_currency/foreign_currency_bloc.dart';
import 'package:xisfo_app/bloc/notify/notify_bloc.dart';
import 'package:xisfo_app/bloc/password/password_bloc.dart';
import 'package:xisfo_app/router/router.dart';
import 'package:xisfo_app/screen/screen.dart';
import 'generated/l10n.dart';
import 'helpers/helpers.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    changeStatusDark();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MultiBlocProvider(
        providers: [
          BlocProvider(create: (context) => NotifyBloc()),
          BlocProvider(create: (context) => ForeignCurrencyBloc()),
          BlocProvider(create: (context) => PasswordBloc()),
          BlocProvider(create: (context) => StepBloc()),
        ],
        child: MaterialApp(
            debugShowCheckedModeBanner: false,
            title: 'Xisfo Fintech',
            routes: RouterApp.router,
            localizationsDelegates: const [
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
              GlobalCupertinoLocalizations.delegate,
              S.delegate
            ],
            supportedLocales: S.delegate.supportedLocales,
            theme: ThemeData(
              fontFamily: 'Comfortaa',
            ).copyWith(
              focusColor: const Color(0xff6C4F92),
              splashColor: const Color(0xff6C4F92),
              primaryColor: const Color(0xff6C4F92),
              canvasColor: const Color(0xFFfafafa),
            ),
            home: AnimatedSplashScreen(
                duration: 2000,
                splash: const SizedBox(
                  width: 100,
                  height: 100,
                  child: Image(
                      image: AssetImage('assets/images/xisfo-loading.gif')),
                ),
                nextScreen: const MainScreen(),
                splashTransition: SplashTransition.fadeTransition,
                pageTransitionType: PageTransitionType.fade,
                backgroundColor: const Color(0xff6C4F92))));
  }
}

class MainScreen extends StatelessWidget {

  const MainScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return OfflineBuilder(
      connectivityBuilder: (
        BuildContext context,
        ConnectivityResult connectivity,
        Widget child,
      ) {
        final bool connected = connectivity == ConnectivityResult.ethernet || connectivity == ConnectivityResult.wifi || connectivity == ConnectivityResult.mobile;
        return connected ? LoginScreen() : OfflineScreen();
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: const [
          Text(
            'There are no bottons to push :)',
          ),
          Text(
            'Just turn off your internet.',
          ),
        ],
      ),
    );
  }
}

// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  static String m0(total) => "Total equity: ${total}";

  static String m1(name) => "${name}, Short reckonings make long friends";

  static String m2(peso) => "The documents must weigh no more than ${peso}";

  static String m3(name) =>
      "Don\'t worry, ${name}! Make your bulk payments here";

  static String m4(value) =>
      "*Dispersion Value(\\\$${value} + VAT among Xisfo wallets)";

  static String m5(phone) => "You asked ${phone}";

  static String m6(Cafer) => "${Cafer}, here is your money";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "Pin": MessageLookupByLibrary.simpleMessage("Pin"),
        "account": MessageLookupByLibrary.simpleMessage("Account"),
        "accountBank": MessageLookupByLibrary.simpleMessage("Bank accounts"),
        "activityEconomic":
            MessageLookupByLibrary.simpleMessage("Economic Activity"),
        "addArchive": MessageLookupByLibrary.simpleMessage("Attach file"),
        "addText": MessageLookupByLibrary.simpleMessage("Join"),
        "alertInvestorConfirm": MessageLookupByLibrary.simpleMessage(
            "100% must be reached in the participation field list.."),
        "appInvestorTotal": m0,
        "ask": MessageLookupByLibrary.simpleMessage("Ask"),
        "asks": MessageLookupByLibrary.simpleMessage("Ask"),
        "asksFriends": MessageLookupByLibrary.simpleMessage(
            "Ask that pal that owes you !"),
        "asksTitle": m1,
        "authData": MessageLookupByLibrary.simpleMessage("Data authentication"),
        "available": MessageLookupByLibrary.simpleMessage("Available"),
        "back": MessageLookupByLibrary.simpleMessage("Back"),
        "changeEmail": MessageLookupByLibrary.simpleMessage("Change Email"),
        "changePin": MessageLookupByLibrary.simpleMessage("Change PIN"),
        "changeTextPin": MessageLookupByLibrary.simpleMessage(
            "Remember that your PIN is secret, do not share it with anyone!"),
        "confirmTextPin": MessageLookupByLibrary.simpleMessage("Confirma PIN"),
        "conventions": MessageLookupByLibrary.simpleMessage("Conventions"),
        "createNewAccount":
            MessageLookupByLibrary.simpleMessage("Create Account"),
        "createPin": MessageLookupByLibrary.simpleMessage("Create your PIN"),
        "declarationOrigenFound": MessageLookupByLibrary.simpleMessage(
            "By clicking here I declare the origin of my funds."),
        "deleteTextAlert": MessageLookupByLibrary.simpleMessage(
            "¿Do you want to remove this notification?"),
        "deleteTextAlert2": MessageLookupByLibrary.simpleMessage(
            "If you delete it, you will not be able to recover it."),
        "descriptionThinkClick": MessageLookupByLibrary.simpleMessage(
            "With your convenience in mind, we develop mass payments. Now dispersing payroll, paying suppliers and sending money to several people at the same time is just a click away!"),
        "desireText":
            MessageLookupByLibrary.simpleMessage("I want to register as:"),
        "detailsAccount":
            MessageLookupByLibrary.simpleMessage("Account details"),
        "detailsEconomicActivity": MessageLookupByLibrary.simpleMessage(
            "Enter details of your economic activity."),
        "downLoadTemplate": MessageLookupByLibrary.simpleMessage(
            "Download the shipping template"),
        "email": MessageLookupByLibrary.simpleMessage("Email"),
        "endMoneyInternational":
            MessageLookupByLibrary.simpleMessage("Finalize Foreign Currency."),
        "endRegister": MessageLookupByLibrary.simpleMessage("End Registration"),
        "endStatementFound":
            MessageLookupByLibrary.simpleMessage("Finalize Fund Statement."),
        "enterCallNumber":
            MessageLookupByLibrary.simpleMessage("Enter the cell number"),
        "enterPinPassword":
            MessageLookupByLibrary.simpleMessage("Enter PIN of the Password"),
        "filter": MessageLookupByLibrary.simpleMessage("Filter"),
        "fintech": MessageLookupByLibrary.simpleMessage("fintech"),
        "forgotPassword":
            MessageLookupByLibrary.simpleMessage("Forgot Password"),
        "forgotPin": MessageLookupByLibrary.simpleMessage("Forgot Pin?"),
        "getInto": MessageLookupByLibrary.simpleMessage("Get Into"),
        "hello": MessageLookupByLibrary.simpleMessage("Hello!"),
        "help": MessageLookupByLibrary.simpleMessage("Help"),
        "historyMovements":
            MessageLookupByLibrary.simpleMessage("Transaction History"),
        "home": MessageLookupByLibrary.simpleMessage("Home"),
        "ifWish": MessageLookupByLibrary.simpleMessage("Yes, I wish!"),
        "infoGeneral":
            MessageLookupByLibrary.simpleMessage("General Information"),
        "investor": MessageLookupByLibrary.simpleMessage("Investors"),
        "investorSubtitle": MessageLookupByLibrary.simpleMessage(
            "List below the shareholders that own more than 5% of the shares."),
        "leyPoliData": MessageLookupByLibrary.simpleMessage(
            "By clicking here and in accordance with the law 1581 of 2012, I accept the Personal Data Treatment Policy"),
        "logout": MessageLookupByLibrary.simpleMessage("Log out"),
        "massiveShipments":
            MessageLookupByLibrary.simpleMessage("Massive Shipments"),
        "maxWeightArchive": m2,
        "mobil": MessageLookupByLibrary.simpleMessage("Mobile"),
        "mobilNotValidate":
            MessageLookupByLibrary.simpleMessage("The cell phone is not valid"),
        "movements": MessageLookupByLibrary.simpleMessage("Movements"),
        "myAccount": MessageLookupByLibrary.simpleMessage("My Account"),
        "myData": MessageLookupByLibrary.simpleMessage("My Data"),
        "nameArchiveNomina": MessageLookupByLibrary.simpleMessage(
            "You must name the file Payroll_(Brand)"),
        "newFintechOur":
            MessageLookupByLibrary.simpleMessage("You are new to our fintech"),
        "newPayment": MessageLookupByLibrary.simpleMessage("New Pyment."),
        "newPaymentSubtitle":
            MessageLookupByLibrary.simpleMessage("There is a new payment"),
        "nextContinue": MessageLookupByLibrary.simpleMessage("Continue"),
        "noIdentification":
            MessageLookupByLibrary.simpleMessage("No. IDENTIFICATION"),
        "notCompliedPayment": m3,
        "notRed":
            MessageLookupByLibrary.simpleMessage("You have no network..."),
        "notWant": MessageLookupByLibrary.simpleMessage("No Want"),
        "noteDispersion": m4,
        "numberMobil": MessageLookupByLibrary.simpleMessage("Number Mobile"),
        "numberPhone": MessageLookupByLibrary.simpleMessage("Phone Number"),
        "password": MessageLookupByLibrary.simpleMessage("Password"),
        "payment": MessageLookupByLibrary.simpleMessage("Payments"),
        "paymentHave": m5,
        "paymentsMassive":
            MessageLookupByLibrary.simpleMessage("Mass Payments"),
        "paymentsRecharges":
            MessageLookupByLibrary.simpleMessage("Payments and Recharges"),
        "personLegal": MessageLookupByLibrary.simpleMessage("Company"),
        "personNature": MessageLookupByLibrary.simpleMessage("Person"),
        "persons": MessageLookupByLibrary.simpleMessage("Persons"),
        "processRejected":
            MessageLookupByLibrary.simpleMessage("Transaction Rejected!"),
        "processRejectedText": MessageLookupByLibrary.simpleMessage(
            "Your transaction failed, please contact our Xisfo administrator.."),
        "profile": MessageLookupByLibrary.simpleMessage("Profile"),
        "pymes": MessageLookupByLibrary.simpleMessage("Pymes"),
        "recoverAccount":
            MessageLookupByLibrary.simpleMessage("Recovery account"),
        "registerAs":
            MessageLookupByLibrary.simpleMessage("Have your registration as"),
        "registerHere": MessageLookupByLibrary.simpleMessage("Register Here"),
        "representLegal":
            MessageLookupByLibrary.simpleMessage("Legal Representative"),
        "request": MessageLookupByLibrary.simpleMessage("Request"),
        "requestCredit": MessageLookupByLibrary.simpleMessage("Credit Request"),
        "send": MessageLookupByLibrary.simpleMessage("Send"),
        "sendFriends": MessageLookupByLibrary.simpleMessage("Send to a pal !"),
        "sendPaymentsMassive":
            MessageLookupByLibrary.simpleMessage("SEND MASS MAILINGS"),
        "services": MessageLookupByLibrary.simpleMessage("Services"),
        "signContract": MessageLookupByLibrary.simpleMessage("Sign Contract"),
        "solicitudExitosa":
            MessageLookupByLibrary.simpleMessage("Successful Application."),
        "startDetailAccount":
            MessageLookupByLibrary.simpleMessage("Enter account details."),
        "textDanger":
            MessageLookupByLibrary.simpleMessage("Unable to register."),
        "textDistrust": MessageLookupByLibrary.simpleMessage(
            "We don\'t distrust you, we just want to trust that it\'s you."),
        "textFavorite":
            MessageLookupByLibrary.simpleMessage("Favorite Recipient:"),
        "textMoney": MessageLookupByLibrary.simpleMessage(
            "Do you want to activate foreign currency ?"),
        "textRecovery": MessageLookupByLibrary.simpleMessage(
            "Enter your corporate email and we will send you the instructions to restore the password"),
        "textRegister":
            MessageLookupByLibrary.simpleMessage("Have your registration as"),
        "textSegmentsPersons":
            MessageLookupByLibrary.simpleMessage("ARE YOU AN SME OR A PERSON?"),
        "textSlideMore":
            MessageLookupByLibrary.simpleMessage("Swipe to learn more ..."),
        "textSuccessFully":
            MessageLookupByLibrary.simpleMessage("Successfully registered."),
        "typeDocument": MessageLookupByLibrary.simpleMessage("Type document"),
        "update": MessageLookupByLibrary.simpleMessage("Update"),
        "updateEmail": MessageLookupByLibrary.simpleMessage("Update Email"),
        "updatePassword":
            MessageLookupByLibrary.simpleMessage("Update Password"),
        "uploadYourSupport":
            MessageLookupByLibrary.simpleMessage("Upload your support"),
        "valueTotal": MessageLookupByLibrary.simpleMessage("Total Value"),
        "wallet": MessageLookupByLibrary.simpleMessage("Wallet"),
        "weLoveAgain":
            MessageLookupByLibrary.simpleMessage("We love to see you again"),
        "welcome": MessageLookupByLibrary.simpleMessage("Wellcome"),
        "welcomeDashboard": m6,
        "welcomeFirstText": MessageLookupByLibrary.simpleMessage("Welcome to"),
        "welcomeSecondText": MessageLookupByLibrary.simpleMessage("Our era"),
        "welcomeSubtitleText":
            MessageLookupByLibrary.simpleMessage("Here we all fit!"),
        "welcomeXisfo":
            MessageLookupByLibrary.simpleMessage("Welcome to Xisfo."),
        "withDraw": MessageLookupByLibrary.simpleMessage("Withdraw"),
        "withdrawal": MessageLookupByLibrary.simpleMessage("Withdrawl"),
        "yesWant": MessageLookupByLibrary.simpleMessage("Yes Want"),
        "youBusiness": MessageLookupByLibrary.simpleMessage("of your company")
      };
}

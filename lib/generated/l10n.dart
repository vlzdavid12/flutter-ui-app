// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values, avoid_escaping_inner_quotes

class S {
  S();

  static S? _current;

  static S get current {
    assert(_current != null,
        'No instance of S was loaded. Try to initialize the S delegate before accessing S.current.');
    return _current!;
  }

  static const AppLocalizationDelegate delegate = AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false)
        ? locale.languageCode
        : locale.toString();
    final localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      final instance = S();
      S._current = instance;

      return instance;
    });
  }

  static S of(BuildContext context) {
    final instance = S.maybeOf(context);
    assert(instance != null,
        'No instance of S present in the widget tree. Did you add S.delegate in localizationsDelegates?');
    return instance!;
  }

  static S? maybeOf(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `¡Hola!`
  String get hello {
    return Intl.message(
      '¡Hola!',
      name: 'hello',
      desc: '',
      args: [],
    );
  }

  /// `¡Bienvenido!`
  String get welcome {
    return Intl.message(
      '¡Bienvenido!',
      name: 'welcome',
      desc: '',
      args: [],
    );
  }

  /// `Bienvenido a`
  String get welcomeFirstText {
    return Intl.message(
      'Bienvenido a',
      name: 'welcomeFirstText',
      desc: '',
      args: [],
    );
  }

  /// `nuestra era`
  String get welcomeSecondText {
    return Intl.message(
      'nuestra era',
      name: 'welcomeSecondText',
      desc: '',
      args: [],
    );
  }

  /// `Fintech`
  String get fintech {
    return Intl.message(
      'Fintech',
      name: 'fintech',
      desc: '',
      args: [],
    );
  }

  /// `¡Aquí cabemos todos!`
  String get welcomeSubtitleText {
    return Intl.message(
      '¡Aquí cabemos todos!',
      name: 'welcomeSubtitleText',
      desc: '',
      args: [],
    );
  }

  /// `Ingresa tu número celular`
  String get numberMobil {
    return Intl.message(
      'Ingresa tu número celular',
      name: 'numberMobil',
      desc: '',
      args: [],
    );
  }

  /// `Celular`
  String get mobil {
    return Intl.message(
      'Celular',
      name: 'mobil',
      desc: '',
      args: [],
    );
  }

  /// `Ingresa el numero celular`
  String get enterCallNumber {
    return Intl.message(
      'Ingresa el numero celular',
      name: 'enterCallNumber',
      desc: '',
      args: [],
    );
  }

  /// `El célular no es valido`
  String get mobilNotValidate {
    return Intl.message(
      'El célular no es valido',
      name: 'mobilNotValidate',
      desc: '',
      args: [],
    );
  }

  /// `Ingresar`
  String get getInto {
    return Intl.message(
      'Ingresar',
      name: 'getInto',
      desc: '',
      args: [],
    );
  }

  /// `Crea una cuenta`
  String get createNewAccount {
    return Intl.message(
      'Crea una cuenta',
      name: 'createNewAccount',
      desc: '',
      args: [],
    );
  }

  /// `¿Eres nuevo en nuestra Fintech?`
  String get newFintechOur {
    return Intl.message(
      '¿Eres nuevo en nuestra Fintech?',
      name: 'newFintechOur',
      desc: '',
      args: [],
    );
  }

  /// `Ingresa tu PIN`
  String get password {
    return Intl.message(
      'Ingresa tu PIN',
      name: 'password',
      desc: '',
      args: [],
    );
  }

  /// `Modificar`
  String get update {
    return Intl.message(
      'Modificar',
      name: 'update',
      desc: '',
      args: [],
    );
  }

  /// `Cambia tu PIN`
  String get updatePassword {
    return Intl.message(
      'Cambia tu PIN',
      name: 'updatePassword',
      desc: '',
      args: [],
    );
  }

  /// `Correo electrónico`
  String get email {
    return Intl.message(
      'Correo electrónico',
      name: 'email',
      desc: '',
      args: [],
    );
  }

  /// `Modifica tu correo`
  String get updateEmail {
    return Intl.message(
      'Modifica tu correo',
      name: 'updateEmail',
      desc: '',
      args: [],
    );
  }

  /// `Enviar`
  String get send {
    return Intl.message(
      'Enviar',
      name: 'send',
      desc: '',
      args: [],
    );
  }

  /// `Pedido`
  String get request {
    return Intl.message(
      'Pedido',
      name: 'request',
      desc: '',
      args: [],
    );
  }

  /// `Paga`
  String get payment {
    return Intl.message(
      'Paga',
      name: 'payment',
      desc: '',
      args: [],
    );
  }

  /// `Retira`
  String get withDraw {
    return Intl.message(
      'Retira',
      name: 'withDraw',
      desc: '',
      args: [],
    );
  }

  /// `Tu cuenta`
  String get myAccount {
    return Intl.message(
      'Tu cuenta',
      name: 'myAccount',
      desc: '',
      args: [],
    );
  }

  /// `Envíos masivos`
  String get massiveShipments {
    return Intl.message(
      'Envíos masivos',
      name: 'massiveShipments',
      desc: '',
      args: [],
    );
  }

  /// `Movimientos`
  String get movements {
    return Intl.message(
      'Movimientos',
      name: 'movements',
      desc: '',
      args: [],
    );
  }

  /// `Pide`
  String get ask {
    return Intl.message(
      'Pide',
      name: 'ask',
      desc: '',
      args: [],
    );
  }

  /// `Atrás`
  String get back {
    return Intl.message(
      'Atrás',
      name: 'back',
      desc: '',
      args: [],
    );
  }

  /// `Inicio`
  String get home {
    return Intl.message(
      'Inicio',
      name: 'home',
      desc: '',
      args: [],
    );
  }

  /// `Billetera`
  String get wallet {
    return Intl.message(
      'Billetera',
      name: 'wallet',
      desc: '',
      args: [],
    );
  }

  /// `Servicios`
  String get services {
    return Intl.message(
      'Servicios',
      name: 'services',
      desc: '',
      args: [],
    );
  }

  /// `Perfil`
  String get profile {
    return Intl.message(
      'Perfil',
      name: 'profile',
      desc: '',
      args: [],
    );
  }

  /// `Disponible`
  String get available {
    return Intl.message(
      'Disponible',
      name: 'available',
      desc: '',
      args: [],
    );
  }

  /// `Ingresa el PIN o la Contraseña`
  String get enterPinPassword {
    return Intl.message(
      'Ingresa el PIN o la Contraseña',
      name: 'enterPinPassword',
      desc: '',
      args: [],
    );
  }

  /// `¿Olvidaste tu PIN?`
  String get forgotPin {
    return Intl.message(
      '¿Olvidaste tu PIN?',
      name: 'forgotPin',
      desc: '',
      args: [],
    );
  }

  /// `Olvidaste tu PIN`
  String get forgotPassword {
    return Intl.message(
      'Olvidaste tu PIN',
      name: 'forgotPassword',
      desc: '',
      args: [],
    );
  }

  /// `Recuperar Cuenta`
  String get recoverAccount {
    return Intl.message(
      'Recuperar Cuenta',
      name: 'recoverAccount',
      desc: '',
      args: [],
    );
  }

  /// `Introduce tu correo electrónico y te enviaremos las instrucciones para restablecer tu PIN`
  String get textRecovery {
    return Intl.message(
      'Introduce tu correo electrónico y te enviaremos las instrucciones para restablecer tu PIN',
      name: 'textRecovery',
      desc: '',
      args: [],
    );
  }

  /// `Haz tu registro como`
  String get textRegister {
    return Intl.message(
      'Haz tu registro como',
      name: 'textRegister',
      desc: '',
      args: [],
    );
  }

  /// `Desliza para conocer más`
  String get textSlideMore {
    return Intl.message(
      'Desliza para conocer más',
      name: 'textSlideMore',
      desc: '',
      args: [],
    );
  }

  /// `Regístrate Aquí`
  String get registerHere {
    return Intl.message(
      'Regístrate Aquí',
      name: 'registerHere',
      desc: '',
      args: [],
    );
  }

  /// `Haz tu registro como`
  String get registerAs {
    return Intl.message(
      'Haz tu registro como',
      name: 'registerAs',
      desc: '',
      args: [],
    );
  }

  /// `¿ERES UNA PYME O PERTENECES AL SEGMENTO PERSONAS?`
  String get textSegmentsPersons {
    return Intl.message(
      '¿ERES UNA PYME O PERTENECES AL SEGMENTO PERSONAS?',
      name: 'textSegmentsPersons',
      desc: '',
      args: [],
    );
  }

  /// `Personas`
  String get persons {
    return Intl.message(
      'Personas',
      name: 'persons',
      desc: '',
      args: [],
    );
  }

  /// `Pymes`
  String get pymes {
    return Intl.message(
      'Pymes',
      name: 'pymes',
      desc: '',
      args: [],
    );
  }

  /// `No desconfiamos de ti, solo queremos confirmar que eres tú`
  String get textDistrust {
    return Intl.message(
      'No desconfiamos de ti, solo queremos confirmar que eres tú',
      name: 'textDistrust',
      desc: '',
      args: [],
    );
  }

  /// `Nos encanta verte de nuevo`
  String get weLoveAgain {
    return Intl.message(
      'Nos encanta verte de nuevo',
      name: 'weLoveAgain',
      desc: '',
      args: [],
    );
  }

  /// `¡{Cafer}, aquí está tu plata!`
  String welcomeDashboard(Object Cafer) {
    return Intl.message(
      '¡$Cafer, aquí está tu plata!',
      name: 'welcomeDashboard',
      desc: '',
      args: [Cafer],
    );
  }

  /// `Solicita tu crédito`
  String get requestCredit {
    return Intl.message(
      'Solicita tu crédito',
      name: 'requestCredit',
      desc: '',
      args: [],
    );
  }

  /// `Pagos y recargas`
  String get paymentsRecharges {
    return Intl.message(
      'Pagos y recargas',
      name: 'paymentsRecharges',
      desc: '',
      args: [],
    );
  }

  /// `Convenios`
  String get conventions {
    return Intl.message(
      'Convenios',
      name: 'conventions',
      desc: '',
      args: [],
    );
  }

  /// `Tus datos`
  String get myData {
    return Intl.message(
      'Tus datos',
      name: 'myData',
      desc: '',
      args: [],
    );
  }

  /// `Tipo documento`
  String get typeDocument {
    return Intl.message(
      'Tipo documento',
      name: 'typeDocument',
      desc: '',
      args: [],
    );
  }

  /// `Si Deseo`
  String get ifWish {
    return Intl.message(
      'Si Deseo',
      name: 'ifWish',
      desc: '',
      args: [],
    );
  }

  /// `Si vas a realizar un proceso de monetización con nosotros debes activar moneda extranjera aquí`
  String get textMoney {
    return Intl.message(
      'Si vas a realizar un proceso de monetización con nosotros debes activar moneda extranjera aquí',
      name: 'textMoney',
      desc: '',
      args: [],
    );
  }

  /// `Cambiar PIN`
  String get changePin {
    return Intl.message(
      'Cambiar PIN',
      name: 'changePin',
      desc: '',
      args: [],
    );
  }

  /// `Ingresa tu nuevo PIN`
  String get confirmTextPin {
    return Intl.message(
      'Ingresa tu nuevo PIN',
      name: 'confirmTextPin',
      desc: '',
      args: [],
    );
  }

  /// `Ingresa tu nuevo correo electrónico`
  String get changeEmail {
    return Intl.message(
      'Ingresa tu nuevo correo electrónico',
      name: 'changeEmail',
      desc: '',
      args: [],
    );
  }

  /// `Recuerda que tu PIN es secreto, ¡no lo compartas con nadie!`
  String get changeTextPin {
    return Intl.message(
      'Recuerda que tu PIN es secreto, ¡no lo compartas con nadie!',
      name: 'changeTextPin',
      desc: '',
      args: [],
    );
  }

  /// `Ingresa tu PIN actual`
  String get Pin {
    return Intl.message(
      'Ingresa tu PIN actual',
      name: 'Pin',
      desc: '',
      args: [],
    );
  }

  /// `No. de Identificación`
  String get noIdentification {
    return Intl.message(
      'No. de Identificación',
      name: 'noIdentification',
      desc: '',
      args: [],
    );
  }

  /// `Persona natural`
  String get personNature {
    return Intl.message(
      'Persona natural',
      name: 'personNature',
      desc: '',
      args: [],
    );
  }

  /// `Persona jurídica`
  String get personLegal {
    return Intl.message(
      'Persona jurídica',
      name: 'personLegal',
      desc: '',
      args: [],
    );
  }

  /// `Deseo inscribirme como:`
  String get desireText {
    return Intl.message(
      'Deseo inscribirme como:',
      name: 'desireText',
      desc: '',
      args: [],
    );
  }

  /// `Cuánto`
  String get valueTotal {
    return Intl.message(
      'Cuánto',
      name: 'valueTotal',
      desc: '',
      args: [],
    );
  }

  /// `Destinatarios favoritos:`
  String get textFavorite {
    return Intl.message(
      'Destinatarios favoritos:',
      name: 'textFavorite',
      desc: '',
      args: [],
    );
  }

  /// `Deseas eliminar esta notificación?`
  String get deleteTextAlert {
    return Intl.message(
      'Deseas eliminar esta notificación?',
      name: 'deleteTextAlert',
      desc: '',
      args: [],
    );
  }

  /// `Si lo eliminas no podrás recuperarlo.`
  String get deleteTextAlert2 {
    return Intl.message(
      'Si lo eliminas no podrás recuperarlo.',
      name: 'deleteTextAlert2',
      desc: '',
      args: [],
    );
  }

  /// `No, Sedeo`
  String get notWant {
    return Intl.message(
      'No, Sedeo',
      name: 'notWant',
      desc: '',
      args: [],
    );
  }

  /// `Sì, Deseo`
  String get yesWant {
    return Intl.message(
      'Sì, Deseo',
      name: 'yesWant',
      desc: '',
      args: [],
    );
  }

  /// `Información básica`
  String get infoGeneral {
    return Intl.message(
      'Información básica',
      name: 'infoGeneral',
      desc: '',
      args: [],
    );
  }

  /// `de tu empresa`
  String get youBusiness {
    return Intl.message(
      'de tu empresa',
      name: 'youBusiness',
      desc: '',
      args: [],
    );
  }

  /// `Continuar`
  String get nextContinue {
    return Intl.message(
      'Continuar',
      name: 'nextContinue',
      desc: '',
      args: [],
    );
  }

  /// `Representante legal`
  String get representLegal {
    return Intl.message(
      'Representante legal',
      name: 'representLegal',
      desc: '',
      args: [],
    );
  }

  /// `Actividad Ecónomica`
  String get activityEconomic {
    return Intl.message(
      'Actividad Ecónomica',
      name: 'activityEconomic',
      desc: '',
      args: [],
    );
  }

  /// `Ingresa los detalles de tu actividad económica`
  String get detailsEconomicActivity {
    return Intl.message(
      'Ingresa los detalles de tu actividad económica',
      name: 'detailsEconomicActivity',
      desc: '',
      args: [],
    );
  }

  /// `Accionistas`
  String get investor {
    return Intl.message(
      'Accionistas',
      name: 'investor',
      desc: '',
      args: [],
    );
  }

  /// `A continuación relacione los accionistas que poseen más del 5% de participación`
  String get investorSubtitle {
    return Intl.message(
      'A continuación relacione los accionistas que poseen más del 5% de participación',
      name: 'investorSubtitle',
      desc: '',
      args: [],
    );
  }

  /// `Total de participación: {total}`
  String appInvestorTotal(Object total) {
    return Intl.message(
      'Total de participación: $total',
      name: 'appInvestorTotal',
      desc: '',
      args: [total],
    );
  }

  /// `Declaración de origen de fondos.`
  String get endStatementFound {
    return Intl.message(
      'Declaración de origen de fondos.',
      name: 'endStatementFound',
      desc: '',
      args: [],
    );
  }

  /// `Finalizar activación de Moneda Extranjera`
  String get endMoneyInternational {
    return Intl.message(
      'Finalizar activación de Moneda Extranjera',
      name: 'endMoneyInternational',
      desc: '',
      args: [],
    );
  }

  /// `¡Bienvenido a Xisfo!`
  String get welcomeXisfo {
    return Intl.message(
      '¡Bienvenido a Xisfo!',
      name: 'welcomeXisfo',
      desc: '',
      args: [],
    );
  }

  /// `Continuar`
  String get nextContinuo {
    return Intl.message(
      'Continuar',
      name: 'nextContinuo',
      desc: '',
      args: [],
    );
  }

  /// `Retiro`
  String get withdrawal {
    return Intl.message(
      'Retiro',
      name: 'withdrawal',
      desc: '',
      args: [],
    );
  }

  /// `Al hacer click aquí Declaro el origen de mis fondos`
  String get declarationOrigenFound {
    return Intl.message(
      'Al hacer click aquí Declaro el origen de mis fondos',
      name: 'declarationOrigenFound',
      desc: '',
      args: [],
    );
  }

  /// `Al hacer click aquí y de acuerdo a la ley 1581 del 2012, acepto la Política de tratamiento de datos personales`
  String get leyPoliData {
    return Intl.message(
      'Al hacer click aquí y de acuerdo a la ley 1581 del 2012, acepto la Política de tratamiento de datos personales',
      name: 'leyPoliData',
      desc: '',
      args: [],
    );
  }

  /// `Firmar Contrato`
  String get signContract {
    return Intl.message(
      'Firmar Contrato',
      name: 'signContract',
      desc: '',
      args: [],
    );
  }

  /// `La sumatoria de la participación de los invesionistas debe ser igual a 100%`
  String get alertInvestorConfirm {
    return Intl.message(
      'La sumatoria de la participación de los invesionistas debe ser igual a 100%',
      name: 'alertInvestorConfirm',
      desc: '',
      args: [],
    );
  }

  /// `Adjuntar`
  String get addText {
    return Intl.message(
      'Adjuntar',
      name: 'addText',
      desc: '',
      args: [],
    );
  }

  /// `Ingresa los detalles de tu cuenta`
  String get startDetailAccount {
    return Intl.message(
      'Ingresa los detalles de tu cuenta',
      name: 'startDetailAccount',
      desc: '',
      args: [],
    );
  }

  /// `¡Despreocúpate, {name}! Realiza tus pagos masivos aquí`
  String notCompliedPayment(Object name) {
    return Intl.message(
      '¡Despreocúpate, $name! Realiza tus pagos masivos aquí',
      name: 'notCompliedPayment',
      desc: '',
      args: [name],
    );
  }

  /// `Pensando en tu comodidad desarrollamos pagos masivos. Ahora dispersar nómina, pagar a proveedores y enviar dinero a varias personas al tiempo, está al alcance de un solo tap`
  String get descriptionThinkClick {
    return Intl.message(
      'Pensando en tu comodidad desarrollamos pagos masivos. Ahora dispersar nómina, pagar a proveedores y enviar dinero a varias personas al tiempo, está al alcance de un solo tap',
      name: 'descriptionThinkClick',
      desc: '',
      args: [],
    );
  }

  /// `Descarga la plantilla de envíos`
  String get downLoadTemplate {
    return Intl.message(
      'Descarga la plantilla de envíos',
      name: 'downLoadTemplate',
      desc: '',
      args: [],
    );
  }

  /// `*Valor dispersión (${value} + IVA entre billeteras Xisfo)`
  String noteDispersion(Object value) {
    return Intl.message(
      '*Valor dispersión (\$$value + IVA entre billeteras Xisfo)',
      name: 'noteDispersion',
      desc: '',
      args: [value],
    );
  }

  /// `Adjuntar archivo`
  String get addArchive {
    return Intl.message(
      'Adjuntar archivo',
      name: 'addArchive',
      desc: '',
      args: [],
    );
  }

  /// `El documento debe pesar máximo {peso}`
  String maxWeightArchive(Object peso) {
    return Intl.message(
      'El documento debe pesar máximo $peso',
      name: 'maxWeightArchive',
      desc: '',
      args: [peso],
    );
  }

  /// `Debes nombrar el archivo Nómina_(Marca)`
  String get nameArchiveNomina {
    return Intl.message(
      'Debes nombrar el archivo Nómina_(Marca)',
      name: 'nameArchiveNomina',
      desc: '',
      args: [],
    );
  }

  /// `REALIZAR PAGO MASIVO`
  String get sendPaymentsMassive {
    return Intl.message(
      'REALIZAR PAGO MASIVO',
      name: 'sendPaymentsMassive',
      desc: '',
      args: [],
    );
  }

  /// `Filtrar`
  String get filter {
    return Intl.message(
      'Filtrar',
      name: 'filter',
      desc: '',
      args: [],
    );
  }

  /// `No tienes red...`
  String get notRed {
    return Intl.message(
      'No tienes red...',
      name: 'notRed',
      desc: '',
      args: [],
    );
  }

  /// `Pide`
  String get asks {
    return Intl.message(
      'Pide',
      name: 'asks',
      desc: '',
      args: [],
    );
  }

  /// `{name}, Las cuentas  claras y el chocolate espeso`
  String asksTitle(Object name) {
    return Intl.message(
      '$name, Las cuentas  claras y el chocolate espeso',
      name: 'asksTitle',
      desc: '',
      args: [name],
    );
  }

  /// `!Pídele a ese parcero que te debe!`
  String get asksFriends {
    return Intl.message(
      '!Pídele a ese parcero que te debe!',
      name: 'asksFriends',
      desc: '',
      args: [],
    );
  }

  /// `Número Celular`
  String get numberPhone {
    return Intl.message(
      'Número Celular',
      name: 'numberPhone',
      desc: '',
      args: [],
    );
  }

  /// `¡Envía a un parcero!`
  String get sendFriends {
    return Intl.message(
      '¡Envía a un parcero!',
      name: 'sendFriends',
      desc: '',
      args: [],
    );
  }

  /// `Se ha registrado correctamente.`
  String get textSuccessFully {
    return Intl.message(
      'Se ha registrado correctamente.',
      name: 'textSuccessFully',
      desc: '',
      args: [],
    );
  }

  /// `No se pudo registrar.`
  String get textDanger {
    return Intl.message(
      'No se pudo registrar.',
      name: 'textDanger',
      desc: '',
      args: [],
    );
  }

  /// `Nuevo Pago.`
  String get newPayment {
    return Intl.message(
      'Nuevo Pago.',
      name: 'newPayment',
      desc: '',
      args: [],
    );
  }

  /// `Hay un nuevo pago`
  String get newPaymentSubtitle {
    return Intl.message(
      'Hay un nuevo pago',
      name: 'newPaymentSubtitle',
      desc: '',
      args: [],
    );
  }

  /// `¡Transacción exitosa!`
  String get solicitudExitosa {
    return Intl.message(
      '¡Transacción exitosa!',
      name: 'solicitudExitosa',
      desc: '',
      args: [],
    );
  }

  /// `Le pediste a {phone}`
  String paymentHave(Object phone) {
    return Intl.message(
      'Le pediste a $phone',
      name: 'paymentHave',
      desc: '',
      args: [phone],
    );
  }

  /// `¡Transacción rechazada!`
  String get processRejected {
    return Intl.message(
      '¡Transacción rechazada!',
      name: 'processRejected',
      desc: '',
      args: [],
    );
  }

  /// `Tu transacción falló, contacta con nuestro administrador Xisfo.`
  String get processRejectedText {
    return Intl.message(
      'Tu transacción falló, contacta con nuestro administrador Xisfo.',
      name: 'processRejectedText',
      desc: '',
      args: [],
    );
  }

  /// `Cuenta`
  String get account {
    return Intl.message(
      'Cuenta',
      name: 'account',
      desc: '',
      args: [],
    );
  }

  /// `Detalles de la cuenta`
  String get detailsAccount {
    return Intl.message(
      'Detalles de la cuenta',
      name: 'detailsAccount',
      desc: '',
      args: [],
    );
  }

  /// `Crea tu PIN`
  String get createPin {
    return Intl.message(
      'Crea tu PIN',
      name: 'createPin',
      desc: '',
      args: [],
    );
  }

  /// `Autenticación de datos`
  String get authData {
    return Intl.message(
      'Autenticación de datos',
      name: 'authData',
      desc: '',
      args: [],
    );
  }

  /// `Finaliza tu registro`
  String get endRegister {
    return Intl.message(
      'Finaliza tu registro',
      name: 'endRegister',
      desc: '',
      args: [],
    );
  }

  /// `Historial Movimientos`
  String get historyMovements {
    return Intl.message(
      'Historial Movimientos',
      name: 'historyMovements',
      desc: '',
      args: [],
    );
  }

  /// `Pagos Masivos`
  String get paymentsMassive {
    return Intl.message(
      'Pagos Masivos',
      name: 'paymentsMassive',
      desc: '',
      args: [],
    );
  }

  /// `Sube tu soporte`
  String get uploadYourSupport {
    return Intl.message(
      'Sube tu soporte',
      name: 'uploadYourSupport',
      desc: '',
      args: [],
    );
  }

  /// `Ayuda`
  String get help {
    return Intl.message(
      'Ayuda',
      name: 'help',
      desc: '',
      args: [],
    );
  }

  /// `Cerrar Sesión`
  String get logout {
    return Intl.message(
      'Cerrar Sesión',
      name: 'logout',
      desc: '',
      args: [],
    );
  }

  /// `Cuentas bancarias`
  String get accountBank {
    return Intl.message(
      'Cuentas bancarias',
      name: 'accountBank',
      desc: '',
      args: [],
    );
  }

  /// `Xisfo Personas`
  String get xisfoPersons {
    return Intl.message(
      'Xisfo Personas',
      name: 'xisfoPersons',
      desc: '',
      args: [],
    );
  }

  /// `Xisfo Pymes`
  String get xisfoPymes {
    return Intl.message(
      'Xisfo Pymes',
      name: 'xisfoPymes',
      desc: '',
      args: [],
    );
  }

  /// `Descargar formato`
  String get downloadFormat {
    return Intl.message(
      'Descargar formato',
      name: 'downloadFormat',
      desc: '',
      args: [],
    );
  }

  /// `¡Legaliza tu pago! Sube tus soportes, {name}`
  String legalPaymentSupport(Object name) {
    return Intl.message(
      '¡Legaliza tu pago! Sube tus soportes, $name',
      name: 'legalPaymentSupport',
      desc: '',
      args: [name],
    );
  }

  /// `Selecciona la plataforma`
  String get selectPlatform {
    return Intl.message(
      'Selecciona la plataforma',
      name: 'selectPlatform',
      desc: '',
      args: [],
    );
  }

  /// `Billetera Abono`
  String get walletBonus {
    return Intl.message(
      'Billetera Abono',
      name: 'walletBonus',
      desc: '',
      args: [],
    );
  }

  /// `Solicitud de pago`
  String get paymentApply {
    return Intl.message(
      'Solicitud de pago',
      name: 'paymentApply',
      desc: '',
      args: [],
    );
  }

  /// `Factura No.`
  String get numberFacture {
    return Intl.message(
      'Factura No.',
      name: 'numberFacture',
      desc: '',
      args: [],
    );
  }

  /// `Subir Soporte`
  String get uploadSupport {
    return Intl.message(
      'Subir Soporte',
      name: 'uploadSupport',
      desc: '',
      args: [],
    );
  }

  /// `Monto USD`
  String get mountDollar {
    return Intl.message(
      'Monto USD',
      name: 'mountDollar',
      desc: '',
      args: [],
    );
  }

  /// `Al hacer click aquí aceptas los términos y condiciones del contrato.`
  String get acceptTermCondition {
    return Intl.message(
      'Al hacer click aquí aceptas los términos y condiciones del contrato.',
      name: 'acceptTermCondition',
      desc: '',
      args: [],
    );
  }

  /// `Agregar Accionista`
  String get addShareHolder {
    return Intl.message(
      'Agregar Accionista',
      name: 'addShareHolder',
      desc: '',
      args: [],
    );
  }

  /// `Contrato Moneda Extranjera.`
  String get contractMoney {
    return Intl.message(
      'Contrato Moneda Extranjera.',
      name: 'contractMoney',
      desc: '',
      args: [],
    );
  }

  /// `Comprobemos que tus datos son legítimos`
  String get verifyDataLegal {
    return Intl.message(
      'Comprobemos que tus datos son legítimos',
      name: 'verifyDataLegal',
      desc: '',
      args: [],
    );
  }

  /// `Empezar ahora`
  String get nowVerificar {
    return Intl.message(
      'Empezar ahora',
      name: 'nowVerificar',
      desc: '',
      args: [],
    );
  }

  /// `Verifica tu identidad`
  String get verifiquemosID {
    return Intl.message(
      'Verifica tu identidad',
      name: 'verifiquemosID',
      desc: '',
      args: [],
    );
  }

  /// `Añadir Cuenta`
  String get addAccount {
    return Intl.message(
      'Añadir Cuenta',
      name: 'addAccount',
      desc: '',
      args: [],
    );
  }

  /// `Añadir cuenta bancaria`
  String get addAccountBank {
    return Intl.message(
      'Añadir cuenta bancaria',
      name: 'addAccountBank',
      desc: '',
      args: [],
    );
  }

  /// `{name}, registra tus cuentas bancarias`
  String registerAccountBank(Object name) {
    return Intl.message(
      '$name, registra tus cuentas bancarias',
      name: 'registerAccountBank',
      desc: '',
      args: [name],
    );
  }

  /// `Recuerda que las cuentas bancarias asociadas a tu billetera deben ser de tu  propiedad, de lo contrario la transacción  será rechazada`
  String get recordBankText {
    return Intl.message(
      'Recuerda que las cuentas bancarias asociadas a tu billetera deben ser de tu  propiedad, de lo contrario la transacción  será rechazada',
      name: 'recordBankText',
      desc: '',
      args: [],
    );
  }

  /// `Banco`
  String get bank {
    return Intl.message(
      'Banco',
      name: 'bank',
      desc: '',
      args: [],
    );
  }

  /// `Tipo de cuenta`
  String get typeAccountBank {
    return Intl.message(
      'Tipo de cuenta',
      name: 'typeAccountBank',
      desc: '',
      args: [],
    );
  }

  /// `Número de cuenta`
  String get numberAccountBank {
    return Intl.message(
      'Número de cuenta',
      name: 'numberAccountBank',
      desc: '',
      args: [],
    );
  }

  /// `¿Estás seguro de que esta cuenta que estás agregando te pertenece?  Recuerda que solo puedes retirar a cuentas propias.`
  String get sureAccountAdd {
    return Intl.message(
      '¿Estás seguro de que esta cuenta que estás agregando te pertenece?  Recuerda que solo puedes retirar a cuentas propias.',
      name: 'sureAccountAdd',
      desc: '',
      args: [],
    );
  }

  /// `Buscar...`
  String get search {
    return Intl.message(
      'Buscar...',
      name: 'search',
      desc: '',
      args: [],
    );
  }

  /// `Retirar monto`
  String get withDrawMount {
    return Intl.message(
      'Retirar monto',
      name: 'withDrawMount',
      desc: '',
      args: [],
    );
  }

  /// `Extranjera`
  String get foreignCurrency {
    return Intl.message(
      'Extranjera',
      name: 'foreignCurrency',
      desc: '',
      args: [],
    );
  }

  /// `Cambiar PIN`
  String get btnChangePIN {
    return Intl.message(
      'Cambiar PIN',
      name: 'btnChangePIN',
      desc: '',
      args: [],
    );
  }

  /// `Tarifas`
  String get rates {
    return Intl.message(
      'Tarifas',
      name: 'rates',
      desc: '',
      args: [],
    );
  }

  /// `Historial de pagos`
  String get historyPayments {
    return Intl.message(
      'Historial de pagos',
      name: 'historyPayments',
      desc: '',
      args: [],
    );
  }

  /// `Información`
  String get information {
    return Intl.message(
      'Información',
      name: 'information',
      desc: '',
      args: [],
    );
  }

  /// `Al aceptar confirmas que todos tus datos son verídicos, recuerda que si tus datos no están bien, tus transferencias no llegarán`
  String get confirmVerificationTransferBank {
    return Intl.message(
      'Al aceptar confirmas que todos tus datos son verídicos, recuerda que si tus datos no están bien, tus transferencias no llegarán',
      name: 'confirmVerificationTransferBank',
      desc: '',
      args: [],
    );
  }

  /// `{name}, porque somos transparentes, conoce tus tarifas.`
  String transferKnow(Object name) {
    return Intl.message(
      '$name, porque somos transparentes, conoce tus tarifas.',
      name: 'transferKnow',
      desc: '',
      args: [name],
    );
  }

  /// `Queremos ayudarte Cafer, aclara tus dudas`
  String get helpTitle {
    return Intl.message(
      'Queremos ayudarte Cafer, aclara tus dudas',
      name: 'helpTitle',
      desc: '',
      args: [],
    );
  }

  /// `En esta categoría las preguntas más frecuentes son:`
  String get subHelpTitle {
    return Intl.message(
      'En esta categoría las preguntas más frecuentes son:',
      name: 'subHelpTitle',
      desc: '',
      args: [],
    );
  }

  /// `Tu billetera`
  String get yourWallet {
    return Intl.message(
      'Tu billetera',
      name: 'yourWallet',
      desc: '',
      args: [],
    );
  }

  /// `Moneda Extranjera`
  String get foreignCurrencyMoney {
    return Intl.message(
      'Moneda Extranjera',
      name: 'foreignCurrencyMoney',
      desc: '',
      args: [],
    );
  }

  /// `Si obras como persona natural es aquí donde te registras`
  String get textPersonNatureRegister {
    return Intl.message(
      'Si obras como persona natural es aquí donde te registras',
      name: 'textPersonNatureRegister',
      desc: '',
      args: [],
    );
  }

  /// `Si tienes una empresa registrada con cámara de comercio es aquí donde te registras`
  String get textBusinessRegister {
    return Intl.message(
      'Si tienes una empresa registrada con cámara de comercio es aquí donde te registras',
      name: 'textBusinessRegister',
      desc: '',
      args: [],
    );
  }

  /// `Partner`
  String get partner {
    return Intl.message(
      'Partner',
      name: 'partner',
      desc: '',
      args: [],
    );
  }

  /// `Activar Business Partner`
  String get activePartner {
    return Intl.message(
      'Activar Business Partner',
      name: 'activePartner',
      desc: '',
      args: [],
    );
  }

  /// `Activar Moneda Extranjera`
  String get activeForeignCurrency {
    return Intl.message(
      'Activar Moneda Extranjera',
      name: 'activeForeignCurrency',
      desc: '',
      args: [],
    );
  }

  /// `Para ser nuestro aliado comercial`
  String get textAllyCommercial {
    return Intl.message(
      'Para ser nuestro aliado comercial',
      name: 'textAllyCommercial',
      desc: '',
      args: [],
    );
  }

  /// `Para procesos de monetización`
  String get textProcessMoney {
    return Intl.message(
      'Para procesos de monetización',
      name: 'textProcessMoney',
      desc: '',
      args: [],
    );
  }

  /// `Envía`
  String get sendTwo {
    return Intl.message(
      'Envía',
      name: 'sendTwo',
      desc: '',
      args: [],
    );
  }

  /// `Actualizar`
  String get changeUpdate {
    return Intl.message(
      'Actualizar',
      name: 'changeUpdate',
      desc: '',
      args: [],
    );
  }

  /// `También puedes:`
  String get youCanAlso {
    return Intl.message(
      'También puedes:',
      name: 'youCanAlso',
      desc: '',
      args: [],
    );
  }

  /// `Ingresa los detalles de tu actividad económica`
  String get detailActivityEconomic {
    return Intl.message(
      'Ingresa los detalles de tu actividad económica',
      name: 'detailActivityEconomic',
      desc: '',
      args: [],
    );
  }

  /// `Contáctenos`
  String get contact {
    return Intl.message(
      'Contáctenos',
      name: 'contact',
      desc: '',
      args: [],
    );
  }

  /// `Detalles del pago`
  String get detailsPayment {
    return Intl.message(
      'Detalles del pago',
      name: 'detailsPayment',
      desc: '',
      args: [],
    );
  }

  /// `Fecha`
  String get date {
    return Intl.message(
      'Fecha',
      name: 'date',
      desc: '',
      args: [],
    );
  }

  /// `Tu pago se ha realizado de manera exitosa`
  String get textPaymentSuccessFully {
    return Intl.message(
      'Tu pago se ha realizado de manera exitosa',
      name: 'textPaymentSuccessFully',
      desc: '',
      args: [],
    );
  }

  /// `No hay notifciaiones.`
  String get noFoundNotify {
    return Intl.message(
      'No hay notifciaiones.',
      name: 'noFoundNotify',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'es'),
      Locale.fromSubtags(languageCode: 'en'),
      Locale.fromSubtags(languageCode: 'fr'),
      Locale.fromSubtags(languageCode: 'it'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    for (var supportedLocale in supportedLocales) {
      if (supportedLocale.languageCode == locale.languageCode) {
        return true;
      }
    }
    return false;
  }
}

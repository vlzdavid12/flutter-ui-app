import 'package:animate_do/animate_do.dart';
import "package:flutter/material.dart";
import 'package:lottie/lottie.dart';

import '../../generated/l10n.dart';

class ScreenAnimationTwo extends StatefulWidget {
  @override
  State<ScreenAnimationTwo> createState() => _ScreenAnimationTwoState();
}

class _ScreenAnimationTwoState extends State<ScreenAnimationTwo> with SingleTickerProviderStateMixin{

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    final internalization = S.of(context);
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children:  [
        Lottie.asset('assets/images/logoxisfo.json', width: 260, repeat: false),
           Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
          Text(internalization.hello, style: const TextStyle(color: Colors.white, fontSize: 50, fontFamily: 'Dongle',  height: 0.8)),
          Swing(
              duration: const Duration(milliseconds: 2000),
              delay: const Duration(milliseconds: 500),
              child: const Text('👋',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 32,
                      fontFamily: 'Dongle',
                      height: 0.6))),

        ]),
        Text(internalization.weLoveAgain,
            style: const TextStyle(color: Colors.white), textAlign: TextAlign.center),
        const SizedBox(
          height: 50,
        ),
        SizedBox(
          width: 300,
          child: Text(internalization.textDistrust,
              style: const TextStyle(color: Colors.white), textAlign: TextAlign.center),
        ),
      ],
    );
  }
}



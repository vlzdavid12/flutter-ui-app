import "package:flutter/material.dart";

class HeaderFormWidget extends StatelessWidget {
  String stepNumber;
  String title;
  String subTitle;

  HeaderFormWidget(
      {required this.stepNumber, required this.title, required this.subTitle});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: const EdgeInsets.only(bottom: 10, top: 10),
      height: 80,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
              margin: const EdgeInsets.only(bottom: 15),
              width: 50,
              child: const Icon(Icons.check_circle,
                  size: 30, color: Color(0xff6C4F92))),
          Text(stepNumber.toString(),
              style: const TextStyle(
                  fontSize: 84,
                  fontFamily: 'Dongle',
                  color: Colors.black54,
                  height: 1.0)),
          const SizedBox(width: 10),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(title,
                    style: const TextStyle(
                        fontSize: 28,
                        fontFamily: 'Dongle',
                        color: Colors.black45,
                        height: 0.6)),
                Text(subTitle,
                    style: const TextStyle(
                        fontSize: 20,
                        fontFamily: 'Dongle',
                        color: Colors.black38,
                        height: 0.8))
              ],
            ),
          ),
        ],
      ),
    );
  }
}

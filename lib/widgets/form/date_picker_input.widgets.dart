import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../ui/input_decorations.ui.dart';

class DatePickerInput extends StatelessWidget {
  final String textLabel;
  final TextEditingController dataInputDate;
  DatePickerInput({Key? key, required this.textLabel, required this.dataInputDate}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final date = DateTime.now();
    return Center(
            child: TextFormField(
          cursorColor: Colors.grey,
          controller: dataInputDate,
          decoration: InputDecorations.generalInputDecoration(
              hinText: '',
              labelText: textLabel,
              prefixIcon: Icons.calendar_month,
              colorError: const Color(0xFFC21839),
              colorInput: Colors.grey),
          readOnly: true,
          validator: (String? value) {
            if (value == null || value.trim().isEmpty) {
              return "Este campo es requerido.";
            }
            return null;
          },

          //set it true, so that user will not able to edit text
          onTap: () async {
            DateTime? pickedDate = await showDatePicker(
                context: context,
                initialDate: date,
                firstDate: DateTime(date.year - 90, date.month, date.day),
                //DateTime.now() - not to allow to choose before today.
                lastDate: DateTime(date.year + 4, date.month, date.day),
                builder: (BuildContext context, Widget? child) {
                  return Theme(
                    data: ThemeData(
                      primarySwatch: Colors.grey,
                      splashColor: Colors.black45,
                      textTheme: const TextTheme(
                        subtitle1: TextStyle(color: Colors.black45),
                        button: TextStyle(color: Colors.black45),
                      ),
                      colorScheme: const ColorScheme.light(
                          primary: Color(0xffEDCF53),
                          onSecondary: Colors.black45,
                          onPrimary: Colors.black54,
                          surface: Colors.black45,
                          onSurface: Colors.black45,
                          secondary: Colors.black45),
                      dialogBackgroundColor: Colors.white,
                    ),
                    child: child ?? const Text(""),
                  );
                });

            if (pickedDate != null) {
              String formattedDate =
                  DateFormat('yyyy-MM-dd').format(pickedDate);
                  dataInputDate.text = formattedDate; //set output date to TextField value.

            } else {
              print("Date is not selected");
            }
          },
        ));
  }
}

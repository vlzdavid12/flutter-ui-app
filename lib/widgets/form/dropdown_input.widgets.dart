import 'package:flutter/material.dart';

import '../../ui/input_decorations.ui.dart';

class DopDownInput extends StatefulWidget {
  final List<String> list;
  final String labelDocument;
  final IconData prefixIcon;
  TextEditingController dropDownSelect;

  DopDownInput({Key? key, required this.list, required this.labelDocument, required this.dropDownSelect, required this.prefixIcon})
      : super(key: key);

  @override
  State<DopDownInput> createState() => _DopDownInputState();
}

class _DopDownInputState extends State<DopDownInput> {
  String dropdownValue = '';
  String labelInput = '';

  @override
  void initState() {
    super.initState();
    dropdownValue = widget.list.first;
    labelInput = widget.labelDocument;
  }

  @override
  Widget build(BuildContext context) {
    return DropdownButtonFormField<String>(
      isExpanded: true,
      value: dropdownValue,
      icon: const Icon(Icons.keyboard_arrow_down),
      elevation: 16,
      style: const TextStyle(color: Colors.black45, fontSize: 18),
      decoration: InputDecorations.generalInputDecoration(
          hinText: '',
          labelText: labelInput,
          prefixIcon: widget.prefixIcon,
          colorError: const Color(0xFFC21839),
          colorInput: Colors.grey),
      onChanged: (String? value) {
        setState(() {
          widget.dropDownSelect.text = value!;
          dropdownValue = value;
        });
      },
      onSaved: (String? value){
        widget.dropDownSelect.text = value!;
      },
      validator: (String? value) {
        if (value == null || value.trim().isEmpty) {
          return "Este campo es requerido.";
        }
        return null;
      },
      items: widget.list.map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(value,
              style: const TextStyle(color: Colors.black54, fontSize: 16)),
        );
      }).toList(),
    );
  }
}

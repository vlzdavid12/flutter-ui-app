import 'package:dotted_border/dotted_border.dart';
import "package:flutter/material.dart";

import '../../generated/l10n.dart';

class UploadInput extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final internalization = S.of(context);
    return  Column(
      children: [
        DottedBorder(
          borderType: BorderType.RRect,
          radius: const Radius.circular(10),
          dashPattern: const [4, 4],
          strokeWidth: 2,
          color: const Color(0xff6C4F92),
          child: MaterialButton(
              minWidth:320,
              height: 60,
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
              disabledColor: const Color(0xFFD2D2D2),
              focusColor: Colors.white,
              splashColor: Colors.white,
              highlightColor: Colors.white,
              elevation: 0,
              color: Colors.white,
              onPressed: () {

              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children:  [
                  const Icon(Icons.cloud_upload, color: Color(0xff6C4F92)),
                  const SizedBox(width: 20.0),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: const [
                      Text('Formato',
                          style: TextStyle(
                              color: Color(0xff6C4F92),
                              fontWeight: FontWeight.w700,
                              height: 1.4)),
                      Text('Subir archivo de dispersión',
                          style: TextStyle(
                              fontSize: 12,
                              color: Color(0xff6C4F92),
                              height: 1.0)),
                    ],
                  )
                ],
              )),
        ),
        Container(
          padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 15),
          decoration: BoxDecoration(
              border: Border.all(color: Colors.black26),
            borderRadius: BorderRadius.circular(10),
            color: Colors.white
          ),
          margin: const EdgeInsets.symmetric(vertical: 20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Icon(Icons.check_circle, color: Colors.black54, size: 35,),
              const SizedBox(width: 20.0),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: const [
                  Text("Nómina_Xisfo.pdf", style: TextStyle(height: 1.5, fontWeight: FontWeight.w700, color: Colors.black54)),
                  Text("Peso 456 KB",  style: TextStyle(height: 1.5, fontSize: 12, color: Color(0xff6C4F92)))
                ],
              )
            ],
          ),
        ),
        ListTile(horizontalTitleGap:0.10, leading: const Icon(Icons.emergency, size: 12,), title: Text(internalization.maxWeightArchive("30 MB"), style: const TextStyle(height: 1.5))),
        ListTile(horizontalTitleGap:0.10, leading: const Icon(Icons.emergency, size: 12,), title: Text(internalization.nameArchiveNomina, style: const TextStyle(height: 1.5)))
      ],
    );
  }
}

import 'package:flutter/material.dart';

class CheckBoxInput extends StatefulWidget {
  final List<String> lisTitles;

  const CheckBoxInput({Key? key, required this.lisTitles}) : super(key: key);

  @override
  State<CheckBoxInput> createState() => _CheckBoxInputState();
}

class _CheckBoxInputState extends State<CheckBoxInput> {
  bool isChecked = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      child: Row(
        children: [
          Checkbox(
            activeColor: const Color(0xff6C4F92),
            checkColor: Colors.white,
            value: isChecked,
            onChanged: (bool? value) {
              setState(() {
                isChecked = value!;
              });
            },
          ),
          Expanded(
            child: ListView.builder(
                physics: const NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: widget.lisTitles.length,
                itemBuilder: (BuildContext context, int index) {
                  return Text(widget.lisTitles[index], style: const TextStyle(color: Colors.black54, fontSize: 12));
                }),
          )
        ],
      ),
    );
  }
}


import 'package:animate_do/animate_do.dart';
import "package:flutter/material.dart";
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:xisfo_app/bloc/foreign_currency/foreign_currency_bloc.dart';

import '../generated/l10n.dart';
import '../models/models.dart';

class Menu extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final mq = MediaQueryData.fromWindow(WidgetsBinding.instance.window);
    return Column(
      children: [mq.viewInsets.bottom == 0.0 ? _Menu() : Container()],
    );
  }
}

class _Menu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final internalization = S.of(context);
    final isForeignCurrency =
        context.watch<ForeignCurrencyBloc>().state.isCurrency;

    final List<BtnMenu> subItemsWallet = [
      BtnMenu(
          title: internalization.sendTwo,
          icon: const ImageIcon(
            AssetImage("assets/images/menu/submenu/arrow_up.png"),
            size: 40,
            color: Color(0xFFFFFFFF),
          ),
          color: const Color(0xff6C4F92),
          actionLink: "send"),
      BtnMenu(
          title: internalization.massiveShipments,
          icon: const ImageIcon(
            AssetImage("assets/images/menu/submenu/envios_masivos.png"),
            size: 40,
            color: Color(0xFFFFFFFF),
          ),
          color: const Color(0xff6C4F92),
          actionLink: "send_massive"),
      BtnMenu(
          title: internalization.ask,
          icon: const ImageIcon(
            AssetImage("assets/images/menu/submenu/arrow_right.png"),
            size: 40,
            color: Color(0xFFFFFFFF),
          ),
          color: const Color(0xff6C4F92),
          actionLink: "request"),
      BtnMenu(
          title: internalization.withDraw,
          icon: const ImageIcon(
            AssetImage("assets/images/menu/submenu/arrow_down.png"),
            size: 40,
            color: Color(0xFFFFFFFF),
          ),
          color: const Color(0xff6C4F92),
          actionLink: "withdraw"),
      BtnMenu(
          title: internalization.movements,
          icon: const ImageIcon(
            AssetImage("assets/images/menu/submenu/movimientos.png"),
            size: 40,
            color: Color(0xFFFFFFFF),
          ),
          color: const Color(0xff6C4F92),
          actionLink: "history"),
    ];

    final List<BtnMenu> subItemsServices = [
      BtnMenu(
          title: internalization.requestCredit,
          icon: const ImageIcon(
            AssetImage("assets/images/menu/submenu/credito.png"),
            size: 40,
            color: Color(0xFFFFFFFF),
          ),
          color: const Color(0xff6C4F92),
          actionLink: 'https://creditop.com/xisfo/'),
      BtnMenu(
          title: internalization.paymentsRecharges,
          icon: const ImageIcon(
            AssetImage("assets/images/menu/submenu/pagos_recargas.png"),
            size: 40,
            color: Color(0xFFFFFFFF),
          ),
          color: const Color(0xff6C4F92),
          actionLink: "send_massive"),
      BtnMenu(
          title: internalization.conventions,
          icon: const ImageIcon(
            AssetImage("assets/images/menu/submenu/convenios.png"),
            size: 40,
            color: Color(0xFFFFFFFF),
          ),
          color: const Color(0xff6C4F92),
          actionLink: "send_massive"),
    ];



    return FadeInUp(
      duration: const Duration(milliseconds: 500),
      child: Container(
        color: Colors.transparent,
        height: 115,
        child: SizedBox(
          width: 420,
          child: Stack(alignment: AlignmentDirectional.bottomCenter, children: [
            Container(
              margin: const EdgeInsets.all(2),
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
              decoration: BoxDecoration(
                  color: const Color(0xff6C4F92),
                  borderRadius: BorderRadius.circular(30)),
              width: 580,
              height: 80,
              child: Stack(children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      children: [
                        IconButton(
                          icon: Image.asset('assets/images/menu/Home.png'),
                          onPressed: () {
                            Navigator.pushNamed(context, 'dashboard');
                          },
                        ),
                        Text(internalization.home,
                            style: const TextStyle(
                                color: Colors.white, height: 0.9, fontSize: 11))
                      ],
                    ),
                    Column(
                      children: [
                        IconButton(
                          icon: Image.asset('assets/images/menu/Wallet.png'),
                          onPressed: () {
                            _showSubmenu(context, subItemsWallet);
                          },
                        ),
                        Text(internalization.wallet,
                            style: const TextStyle(
                                color: Colors.white, height: 0.9, fontSize: 11))
                      ],
                    ),
                    Container(
                      width: 60,
                    ),
                    Column(
                      children: [
                        IconButton(
                          icon: Image.asset('assets/images/menu/Plus.png'),
                          onPressed: () {
                            _showSubmenu(context, subItemsServices);
                          },
                        ),
                        Text(internalization.services,
                            style: const TextStyle(
                                color: Colors.white, height: 0.9, fontSize: 11))
                      ],
                    ),
                    BntServiceValidator(option: isForeignCurrency)
                  ],
                ),
              ]),
            ),
            Positioned(
              top: -25,
              height: 155,
              child: SpinPerfect(
                child: IconButton(
                  iconSize: 98,
                  icon: Image.asset('assets/images/menu/icon_xisfo.png'),
                  onPressed: () {},
                ),
              ),
            ),
            const Positioned(
              bottom: 18,
              child: Text('App Xisfo',
                  style: TextStyle(color: Colors.white, fontSize: 11)),
            )
          ]),
        ),
      ),
    );
  }
}

class BntServiceValidator extends StatelessWidget {
  final String option;


  BntServiceValidator(
      {Key? key, required this.option})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final internalization = S.of(context);

    final List<BtnMenu> subItemsForeignCurrency = [
      BtnMenu(
          title: internalization.uploadYourSupport,
          icon: const ImageIcon(
            AssetImage("assets/images/menu/submenu/upload_support.png"),
            size: 40,
            color: Color(0xFFFFFFFF),
          ),
          color: const Color(0xff6C4F92),
          actionLink: ""),
      BtnMenu(
          title: internalization.rates,
          icon: const ImageIcon(
            AssetImage("assets/images/menu/submenu/rates.png"),
            size: 40,
            color: Color(0xFFFFFFFF),
          ),
          color: const Color(0xff6C4F92),
          actionLink: "rates"),
      BtnMenu(
          title: internalization.historyPayments,
          icon: const ImageIcon(
            AssetImage("assets/images/menu/submenu/history_money.png"),
            size: 40,
            color: Color(0xFFFFFFFF),
          ),
          color: const Color(0xff6C4F92),
          actionLink: ""),
      BtnMenu(
          title: internalization.information,
          icon: const ImageIcon(
            AssetImage("assets/images/menu/submenu/im_information.png"),
            size: 40,
            color: Color(0xFFFFFFFF),
          ),
          color: const Color(0xff6C4F92),
          actionLink: ""),
    ];

    switch (option) {
      case 'business_partner':
        {
          return Column(
            children: [
              IconButton(
                icon: Image.asset('assets/images/menu/partner.png'),
                iconSize: 20,
                onPressed: () {
                  _showSubmenu(context, subItemsForeignCurrency);
                },
              ),
              Text(internalization.partner,
                  style: const TextStyle(
                      color: Colors.white, height: 0.9, fontSize: 11))
            ],
          );
        }
        break;
      case 'foreign_currency':
        {
          return Column(
            children: [
              IconButton(
                icon: Image.asset('assets/images/menu/Money.png'),
                onPressed: () {
                  _showSubmenu(context, subItemsForeignCurrency);
                },
              ),
              Text(internalization.foreignCurrency,
                  style: const TextStyle(
                      color: Colors.white, height: 0.9, fontSize: 11))
            ],
          );
        }
        break;
      default:
        {
          return Column(
            children: [
              IconButton(
                icon: Image.asset('assets/images/menu/User.png'),
                onPressed: () {
                  Navigator.pushNamed(context, 'profile');
                },
              ),
              Text(internalization.profile,
                  style: const TextStyle(
                      color: Colors.white, height: 0.9, fontSize: 11))
            ],
          );
        }
        break;
    }
  }
}

_showSubmenu(context, List<BtnMenu> subItems) {
  final size = MediaQuery.of(context).size;
  final internalization = S.of(context);
  showDialog(
      context: context,
      builder: (BuildContext context) {
        return Container(
          color: const Color(0xff6C4F92).withOpacity(0.80),
          child: Dialog(
              alignment: AlignmentDirectional.bottomCenter,
              elevation: 0,
              backgroundColor: Colors.transparent,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0)),
              child: Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                          margin: const EdgeInsets.only(bottom: 10),
                          child: Image.asset('assets/images/logo-white.png',
                              width: 100)),
                      SizedBox(
                        width: double.infinity,
                        height: size.height / 1.6,
                        child: ListView.builder(
                            shrinkWrap: true,
                            itemCount: subItems.length,
                            itemBuilder: (BuildContext context, int index) {
                              return _SubMenu(subItems[index]);
                            }),
                      ),
                      SizedBox(
                        width: double.infinity,
                        child: IconButton(
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            icon: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                const Icon(Icons.arrow_back_ios_new,
                                    size: 14, color: Colors.white),
                                Text(internalization.back,
                                    style: const TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w700,
                                        fontSize: 14))
                              ],
                            )),
                      ),
                    ],
                  ))),
        );
      });
}

class _SubMenu extends StatelessWidget {
  final BtnMenu item;

  const _SubMenu(this.item);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 5),
      padding: const EdgeInsets.symmetric(horizontal: 4, vertical: 4),
      decoration: BoxDecoration(
          color: item.color,
          border: Border.all(width: 1.0, color: item.color),
          borderRadius: BorderRadius.circular(10)),
      width: double.infinity,
      height: 90,
      child: IconButton(
        icon: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            item.icon,
            Text(item.title,
                style: const TextStyle(
                    fontSize: 10, height: 2.0, color: Colors.white))
          ],
        ),
        onPressed: () async {
          if (item.actionLink.contains('https')) {
            var link = item.actionLink.split("/");
            var path = "";
            link.skip(3).forEach((element) {
              path += "/$element";
            });

            final Uri toLaunch = Uri(
                scheme: link[0].replaceAll(":", ""), host: link[2], path: path);
            _launchInBrowser(toLaunch);
          } else {
            Navigator.pushNamed(context, item.actionLink);
          }
        },
      ),
    );
  }
}

Future<void> _launchInBrowser(Uri url) async {
  if (!await launchUrl(
    url,
    mode: LaunchMode.externalApplication,
  )) {
    throw Exception('Could not launch $url');
  }
}

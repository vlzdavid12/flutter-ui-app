import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Slideshow extends StatelessWidget {

  final List<Widget> slides;
  final bool poinstUp;
  final Color colorPrimary;
  final Color colorSecondary;
  final double bulletPrimary;
  final double bulletSecondary;

  const Slideshow({
    super.key,
    required this.slides,
    this.poinstUp     = false,
    this.colorPrimary    = Colors.yellow,
    this.colorSecondary  = Colors.grey,
    this.bulletPrimary   = 12.0,
    this.bulletSecondary = 12.0,
  });


  @override
  Widget build(BuildContext context) {


    return ChangeNotifierProvider(
      create: (_) => _SlideshowModel(),
      child: SafeArea(
        child: Center(
            child: Builder(
              builder: (BuildContext context) {

                Provider.of<_SlideshowModel>(context).colorPrimary   = colorPrimary;
                Provider.of<_SlideshowModel>(context).colorSecondary = colorSecondary;

                Provider.of<_SlideshowModel>(context).bulletPrimary   = bulletPrimary;
                Provider.of<_SlideshowModel>(context).bulletSecondary = bulletSecondary;

                return _CreateLayoutSlideshow(poinstUp: poinstUp, slides: slides);
              },
            )
        ),
      ),
    );

  }
}

class _CreateLayoutSlideshow extends StatelessWidget {
  const _CreateLayoutSlideshow({
    required this.poinstUp,
    required this.slides,
  });

  final bool poinstUp;
  final List<Widget> slides;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        if ( poinstUp )
          _Dots( slides.length ),

        Expanded(
          flex: 1,
            child: _Slides( slides )
        ),

        if ( !poinstUp )
          _Dots( slides.length ),
      ],
    );
  }
}



class _Dots extends StatelessWidget {

  final int totalSlides;

  const _Dots( this.totalSlides );

  @override
  Widget build(BuildContext context) {
    final ssModel = Provider.of<_SlideshowModel>(context);
    return Container(
      color: ssModel.currentPage == 1 ? const Color(0xff6C4F92) : Colors.white,
      child: SizedBox(
        width: double.infinity,
        height: 20,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: List.generate( totalSlides, (i) => _Dot(i) ),
        ),
      ),
    );
  }
}

class _Dot extends StatelessWidget {

  final int index;

  const _Dot( this.index );

  @override
  Widget build(BuildContext context) {

    final ssModel = Provider.of<_SlideshowModel>(context);
    double sizeBox;
    Color color;

    if ( ssModel.currentPage >= index - 0.5 && ssModel.currentPage < index + 0.5 ) {

      sizeBox = ssModel.bulletPrimary;
      color  = ssModel.colorPrimary;

    } else {

      sizeBox = ssModel.bulletSecondary;
      color  = ssModel.currentPage == 1 ? Colors.white : const Color(0xff6C4F92) ;
    }

    return AnimatedContainer(
      duration: const Duration( milliseconds: 200 ),
      width: 40,
      height: sizeBox,
      margin: const EdgeInsets.symmetric( horizontal: 5 ),
      decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(10)
      ),
    );
  }
}




class _Slides extends StatefulWidget {

  final List<Widget> slides;

  const _Slides( this.slides );

  @override
  __SlidesState createState() => __SlidesState();
}

class __SlidesState extends State<_Slides> {

  final pageViewController = PageController(initialPage: 1);

  @override
  void initState() {
    super.initState();

    pageViewController.addListener(() {
      Provider.of<_SlideshowModel>(context, listen: false).currentPage = pageViewController.page!;
    });

  }

  @override
  void dispose() {
    pageViewController.dispose();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    final ssModel = Provider.of<_SlideshowModel>(context);
    return Container(
      color: ssModel.currentPage == 1 ? const Color(0xff6C4F92) : Colors.white,
      child: PageView(
        controller: pageViewController,
        // children: <Widget>[
        //   _Slide( 'assets/svgs/slide-1.svg' ),
        //   _Slide( 'assets/svgs/slide-2.svg' ),
        //   _Slide( 'assets/svgs/slide-3.svg' ),
        // ],
        children: widget.slides.map( (slide) => _Slide( slide ) ).toList(),
      ),
    );
  }
}

class _Slide extends StatelessWidget {

  final Widget slide;

  const  _Slide( this.slide );

  @override
  Widget build(BuildContext context) {
    return Container(
        width: double.infinity,
        height: double.infinity,
        padding: const EdgeInsets.all(30),
        child: slide
    );
  }
}



class _SlideshowModel with ChangeNotifier{

  double _currentPage     = 1;
  Color _colorPrimary    = Colors.yellow;
  Color _colorSecondary  = Colors.grey;
  double _bulletPrimary   = 12;
  double _bulletSecondary = 12;

  double get currentPage => _currentPage;

  set currentPage( double pagina ) {
    _currentPage = pagina;
    notifyListeners();
  }

  Color get colorPrimary => _colorPrimary;
  set colorPrimary( Color color ) {
    _colorPrimary = color;
  }

  Color get colorSecondary => _colorSecondary;
  set colorSecondary( Color color ) {
    _colorSecondary = color;
  }

  double get bulletPrimary => _bulletPrimary;
  set bulletPrimary( double sizeBox ) {
    _bulletPrimary = sizeBox;
  }

  double get bulletSecondary => _bulletSecondary;
  set bulletSecondary( double sizeBox ) {
    _bulletSecondary = sizeBox;
  }

}



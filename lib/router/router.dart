import 'package:xisfo_app/screen/business_partner/business_partner.screen.dart';
import 'package:xisfo_app/screen/foreign_currency/register_wallet.dart';
import 'package:xisfo_app/screen/screen.dart';

class RouterApp {
  static final router = {
    "login":(_) => LoginScreen(),
    "register":(_) => RegisterScreen(),
    "recovery":(_) => RecoveryPass(),
    "dashboard":(_) => DashboardScreen(),
    "profile":(_) => ProfileScreen(),
    "request":(_) => RequestScreen(),
    "send":(_) => SendScreen(),
    "withdraw":(_) => WithDrawScreen(),
    "history": (_) => HistoryScreen(),
    "bank": (_) => AccountBankScreen(),
    "help": (_) => HelpScreen(),
    "pymes_register": (_) => PymesRegisterScreen(),
    "persons_register": (_) => PersonsRegisterScreen(),
    "person_juridica": (_) => PersonJuridicaScreen(),
    "person_natural": (_) => PersonNaturalScreen(),
    "register_business_partner": (_) => RegisterBusinessPartnerScreen(),
    "business_partner": (_) => BusinessPartnerScreen(),
    "send_massive": (_) => SendMassiveScreen(),
    "upload_support": (_) => UploadSupport(),
    "notify": (_) => NotifyListScreen(),
    "rates": (_) => RatesScreen(),
    "options_subscription": (_) =>  RegisterWallet(),
    "preview_screen_invoice": (_) =>  PreviewInvoiceScreen(),
  };
}

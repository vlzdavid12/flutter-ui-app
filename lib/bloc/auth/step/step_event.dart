part of 'step_bloc.dart';

abstract class StepEvent extends Equatable {
  const StepEvent();

  @override
  List<Object> get props => [];
}

class SetChangeStepEvent extends StepEvent{
  final bool newStep;
  const SetChangeStepEvent({required this.newStep});

  @override
  String toString() {
    return 'SetChangeStepEvent{newStep: $newStep}';
  }

  @override
  List<Object> get props => [newStep];

}

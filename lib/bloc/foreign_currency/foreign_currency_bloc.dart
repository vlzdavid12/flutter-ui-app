import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'foreign_currency_event.dart';
part 'foreign_currency_state.dart';

class ForeignCurrencyBloc extends Bloc<ForeignCurrencyEvent, ForeignCurrencyState> {
  ForeignCurrencyBloc() : super(ForeignCurrencyState.initial()) {
    on<IsCurrencyEvent>(_isCurrencyWallet);
  }

  void _isCurrencyWallet(IsCurrencyEvent event, Emitter<ForeignCurrencyState> emit){
    emit(state.copyWith(isCurrency: event.isCurrency));
  }
}

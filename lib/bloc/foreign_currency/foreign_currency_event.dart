part of 'foreign_currency_bloc.dart';

abstract class ForeignCurrencyEvent extends Equatable {
  const ForeignCurrencyEvent();
}

class IsCurrencyEvent  extends ForeignCurrencyEvent {
  final String isCurrency;
  const IsCurrencyEvent({required this.isCurrency});

  @override
  // TODO: implement props
  List<Object?> get props => [isCurrency];
}

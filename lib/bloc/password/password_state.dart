part of 'password_bloc.dart';

class PasswordState extends Equatable {
  final visiblePass;

  const PasswordState({
    required this.visiblePass
  });

  factory PasswordState.initial(){
    return const PasswordState(visiblePass: true);
  }

  @override
  List<Object> get props => [visiblePass];

  @override
  String toString() {
    return 'PasswordState{visiblePass: $visiblePass}';
  }

  PasswordState copyWith({
    bool? visiblePass
  }) {
    return PasswordState(
        visiblePass: visiblePass ?? this.visiblePass,
    );
  }
}

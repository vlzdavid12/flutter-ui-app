part of 'investor_bloc.dart';

abstract class InvestorEvent extends Equatable {
  const InvestorEvent();

  @override
  List<Object> get props => [];
}

class AddInvestorEvent extends InvestorEvent {
  final String investor;
  final String dateExpedition;
  final String stake;
  final String numberIdentification;
  final bool peps;

  const AddInvestorEvent({required this.dateExpedition, required this.stake, required this.numberIdentification, required this.peps, required this.investor});

  @override
  String toString() {
    return 'AddInvestorEvent(investor: $investor, dateExpedition: $dateExpedition, stake: $stake, numberIdentification: $numberIdentification, peps: $peps)';
  }

  @override
  List<Object> get props => [investor, dateExpedition, stake, numberIdentification, peps];

}


class RemoveInvestorEvent extends InvestorEvent {
  final ListInvestor investor;
  const RemoveInvestorEvent({required this.investor});

  @override
  String toString() {
    return 'RemoveInvestorEvent{investor: $investor}';
  }

  @override
  List<Object> get props => [investor];
}

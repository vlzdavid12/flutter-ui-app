part of 'notify_bloc.dart';

class NotifyState extends Equatable {
  List<ListNotify> notify;
  final now = DateTime.now();
  NotifyState({required this.notify});

  factory NotifyState.initial() => NotifyState(notify: [
    ListNotify(id: "6", date: DateTime(DateTime.now().year), title: "titulo 1", description: "subtitle"),
    ListNotify(id: "5", date: DateTime(DateTime.now().year - 2), title: "titulo 2", description: "subtitle"),
    ListNotify(id: "4", date: DateTime(DateTime.now().year - 2), title: "titulo 2", description: "subtitle"),
    ListNotify(id: "3", date: DateTime(DateTime.now().year - 2), title: "titulo 2", description: "subtitle"),
    ListNotify(id: "2", date: DateTime(DateTime.now().year - 3), title: "titulo 2", description: "subtitle"),
    ListNotify(id: "1", date: DateTime(DateTime.now().year - 4), title: "titulo 3", description: "subtitle")
  ]);

  @override
  List<Object?> get props => [notify];


  NotifyState copyWith({List<ListNotify>? notify}) {
    return NotifyState(notify: notify ?? this.notify);
  }

  @override
  String toString() {
    return 'NotifyState{notify: $notify}';
  }
}



part of 'notify_bloc.dart';

abstract class NotifyEvent extends Equatable {
  const NotifyEvent();

  @override
  List<Object> get props => [];
}

class RemoveNotify extends NotifyEvent {
  final ListNotify notify;

  const RemoveNotify({required this.notify});

  @override
  String toString() {
    return 'RemoveNotify{notify: $notify}';
  }

  @override
  List<Object> get props => [notify];

}

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:xisfo_app/models/list_notify.model.dart';

part 'notify_event.dart';
part 'notify_state.dart';

class NotifyBloc extends Bloc<NotifyEvent, NotifyState> {
  NotifyBloc() : super(NotifyState.initial()) {
    on<RemoveNotify>(_removeInvestor);
  }

  void _removeInvestor(RemoveNotify event, Emitter<NotifyState> emit){
    final newInvestor =  state.notify.where((element) => element.id != event.notify.id).toList();
    emit(state.copyWith(notify: newInvestor));
  }
}

class ItemAccordion {
  String expandedValue;
  String headerValue;
  bool isExpanded;

  ItemAccordion({
    required this.expandedValue,
    required this.headerValue,
    this.isExpanded = false,
  });
}

import 'package:equatable/equatable.dart';
import 'package:xisfo_app/models/list_investor.model.dart';

class ListBank extends Equatable{
  final String id;
  final String nameBank;
  final String typeAccount;
  final String numberAccountBank;

  ListBank({
    String? id,
    required this.nameBank,
    required this.typeAccount,
    required this.numberAccountBank,
}): id = id ?? uuid.v4();

  @override
  List<Object?> get props => [id, nameBank, typeAccount, numberAccountBank];

  @override
  String toString() {
    return 'ListBank{id: $id, nameBank: $nameBank, typeAccount: $typeAccount, numberAccountBank: $numberAccountBank}';
  }
}
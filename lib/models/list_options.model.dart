import 'package:equatable/equatable.dart';

class Options extends Equatable{
  final String title;
  const Options({required this.title});

  @override
  // TODO: implement props
  List<Object?> get props => [title];

  @override
  String toString() {
    return 'Options{title: $title}';
  }
}
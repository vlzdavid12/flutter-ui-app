

import 'package:equatable/equatable.dart';
import 'package:uuid/uuid.dart';

Uuid uuid = const Uuid();

class ListInvestor extends Equatable{
  final String id;
  final String investor;
  final String dateExpedition;
  final String stake;
  final String numberIdentification;
  final bool peps;

  ListInvestor({String? id, required this.investor, required this.dateExpedition, required this.stake, required this.numberIdentification, required this.peps}): id = id ?? uuid.v4();

  @override
  // TODO: implement props
  List<Object> get props => [id, investor, dateExpedition, stake, numberIdentification, peps];

  @override
  String toString() {
    return 'ListInvestor(id: $id, investor: $investor, dateExpedition: $dateExpedition, stake: $stake, numberIdentification: $numberIdentification, peps: $peps)';
  }
}
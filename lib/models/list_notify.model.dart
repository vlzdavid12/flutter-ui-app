import 'package:equatable/equatable.dart';

class ListNotify extends Equatable{

  final String id;
  final DateTime date;
  final String title;
  final String description;

  const ListNotify({required this.id, required this.date, required this.title, required this.description});

  @override
  List<Object?> get props => [id, date, title, description];

  @override
  String toString() {
    return 'ListNotify{id: $id, date: $date, title: $title, description: $description}';
  }
}
